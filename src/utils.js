import { take, put } from 'redux-saga/effects'

import {
    has, pick, isArray, reduce, round, get,
    map, filter, isEmpty, find
} from 'lodash'

const LATITUDE_LENGTH = 111.32 * 1000

function* watchChannel(channel) {
    while (true) {
        const action = yield take(channel)
        yield put(action)
    }
}

function latLng(data) {
    const parseData = (data) => {
        if (has(data, 'latitude') && has(data, 'longitude')) {
            return pick(data, ['latitude', 'longitude'])
        }
        if (has(data, 'lat') && has(data, 'lng')) {
            return {
                latitude  : data.lat,
                longitude : data.lng
            }
        }
        if (has(data, 'coords.latitude') && has(data, 'coords.longitude')) {
            return pick(data.coords, ['latitude', 'longitude'])
        }

        if (has(data, 'position.latitude') && has(data, 'position.longitude')) {
            return pick(data.position, ['latitude', 'longitude'])
        }

        if (isArray(data)) {
            return {
                latitude  : data[1],
                longitude : data[0]
            }
        }

        return null
    }

    return reduce(parseData(data), (acc, value, key) => ({
        ...acc,
        [key]: round(value, 6)
    }), {})
}

function regionWithRadius({ latitude, longitude, radius }) {
    const distance = radius * 2 * 1000
    const latitudeDelta = round(distance / LATITUDE_LENGTH, 8)
    const longitudeDelta = round(distance / longitudeLength(latitude), 8)

    const region = {
        latitude,
        longitude,
        latitudeDelta,
        longitudeDelta
    }

    return region
}

function radiusFromRegion(region) {
    if (!region) {
        return null
    }

    const { longitudeDelta, latitude } = region
    return round((longitudeLength(latitude) / 1000) * (longitudeDelta / 2))
}

function longitudeLength(latitude) {
    return Math.cos(toRadians(latitude)) * LATITUDE_LENGTH
}

function toRadians(degrees) {
    return (degrees * Math.PI) / 180
}

function didChangeZoom(regionA, regionB) {
    const zoomDeltaTreshold = 0.0005
    const latDelta = Math.abs(get(regionA, 'latitudeDelta') - get(regionB, 'latitudeDelta')).toFixed(8)
    return latDelta > zoomDeltaTreshold
}

function asMarkers(items) {
    const markers = map(items, (item) => ({
        ...pick(item, ['id', 'name']),
        position: latLng(item)
    }))

    return filter(markers, (marker) => !isEmpty(marker.position))
}

function getCurrentUserReaction(currentUser, event) {
    return find(
        get(event, 'user_reactions', []),
        (reaction) => {
            if (reaction.user.id === currentUser.id) {
                return reaction
            }

            return null
        }
    )
}

function getReactionsNumber(event) {
    return reduce(get(event, 'user_reactions', []),
        (acc, reaction) => {
            if (reaction.going === true) {
                return {
                    ...acc,
                    going: acc.going + 1
                }
            }
            if (reaction.interested === true) {
                return {
                    ...acc,
                    interested: acc.interested + 1
                }
            }
            return acc
        },
        { going: 0, interested: 0 })
}

export default {
    watchChannel,
    latLng,
    radiusFromRegion,
    regionWithRadius,
    didChangeZoom,
    asMarkers,
    getCurrentUserReaction,
    getReactionsNumber
}
