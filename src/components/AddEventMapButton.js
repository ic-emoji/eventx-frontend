import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet, ViewPropTypes } from 'react-native'
import { connect } from 'react-redux'

import { routing } from '../redux'

import IconButton from './IconButton'
import CrossHair from './CrossHair'

import Colors from '../colors'

class AddEventMapButton extends Component {
    constructor(props) {
        super(props)
        this.state = { isLocationActive: false }
    }

    activateLocation = () => {
        this.setState({ isLocationActive: true })
    }

    addEvent = () => {
        const { dispatch } = this.props

        dispatch(routing.navigate({ routeName: 'eventView', params: { isForm: true } }))
        this.setState({ isLocationActive: false })
    }

    render() {
        const { isLocationActive } = this.state
        const { style } = this.props
        return (
            <Fragment>
                <View style={ [styles.container, style, styles.event, isLocationActive && styles.active] }>
                    <IconButton
                        color={ isLocationActive ? Colors.white : Colors.darkGrey }
                        onPress={ isLocationActive ? this.addEvent : this.activateLocation }
                        icon='sf/add'
                        iconSize={ 22 }
                    />
                </View>
                { isLocationActive && <CrossHair /> }
            </Fragment>
        )
    }
}

AddEventMapButton.propTypes = {
    style    : ViewPropTypes.style,
    dispatch : PropTypes.func
}

const styles = StyleSheet.create({
    container: {
        position        : 'absolute',
        width           : 40,
        height          : 40,
        borderRadius    : 20,
        backgroundColor : Colors.white,
        shadowColor     : Colors.black,
        shadowOpacity   : 0.1,
        shadowOffset    : { width: 2, height: 4 },
        shadowRadius    : 4,
        elevation       : 3,
        alignItems      : 'center',
        justifyContent  : 'center'
    },
    active: {
        backgroundColor: Colors.blue
    },
    event: {
        right : 12,
        top   : 82
    }
})

export default connect()(AddEventMapButton)
