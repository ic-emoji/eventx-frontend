import React from 'react'
import PropTypes from 'prop-types'
import { onlyUpdateForKeys } from 'recompose'
import { View, StyleSheet } from 'react-native'
import { KeyboardAccessoryView } from 'react-native-keyboard-accessory'

import Button from './Button'

const StickyButton = (props) => {
    const { title, icon, onPress, isDisabled, isSubmitting, ...otherProps } = props
    return (
        <KeyboardAccessoryView
            style={ styles.container }
            alwaysVisible
            hideBorder
        >
            <View style={ styles.wrapper }>
                <Button
                    onPress={ onPress }
                    isLoading={ isSubmitting }
                    isDisabled={ isDisabled }
                    title={ title }
                    icon={ icon }
                    { ...otherProps }
                />
            </View>
        </KeyboardAccessoryView>
    )
}


StickyButton.propTypes = {
    title        : PropTypes.string,
    icon         : PropTypes.string,
    onPress      : PropTypes.func,
    isSubmitting : PropTypes.bool,
    isDisabled   : PropTypes.bool
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent'
    },
    wrapper: {
        height            : 70,
        paddingHorizontal : 15,
        backgroundColor   : 'transparent'
    }
})

export default onlyUpdateForKeys(['title', 'icon', 'isDisabled', 'isSubmitting'])(StickyButton)
