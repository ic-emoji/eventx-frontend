import React from 'react'
import PropTypes from 'prop-types'

import { values } from 'lodash'

import { TouchableOpacity, StyleSheet } from 'react-native'
import { ui } from '../redux'

import Text, { WEIGHT } from './Text'

import Colors from '../colors'

const Toast = ({ title, intent, message, onClose }) => (
    <TouchableOpacity style={ [styles.container, styles[intent]] } onPress={ onClose }>
        <Text weight={ WEIGHT.bold } style={ styles.title }>{ title }</Text>
        <Text style={ styles.message }>{ message }</Text>
    </TouchableOpacity>
)

Toast.propTypes = {
    title   : PropTypes.string,
    message : PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
    intent  : PropTypes.oneOf(values(ui.INTENT)).isRequired,
    onClose : PropTypes.func.isRequired
}

const styles = StyleSheet.create({
    container: {
        flex             : 1,
        minHeight        : 50,
        padding          : 20,
        marginHorizontal : 15,
        marginBottom     : 15,
        borderRadius     : 5,
        shadowColor      : Colors.black,
        shadowOpacity    : 0.2,
        shadowOffset     : { width: 0, height: 6 },
        shadowRadius     : 24,
        elevation        : 4
    },
    title: {
        fontSize     : 16,
        color        : Colors.white,
        marginBottom : 10
    },
    message: {
        color: Colors.white
    },
    primary: {
        backgroundColor: Colors.blue
    },
    success: {
        backgroundColor: Colors.green
    },
    danger: {
        backgroundColor: Colors.red
    },
    warning: {
        backgroundColor: Colors.orange
    },
    default: {
        backgroundColor: Colors.lightGrey
    }
})

export default Toast
