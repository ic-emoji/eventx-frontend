import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Carousel from 'react-native-snap-carousel'

import { Dimensions, StyleSheet } from 'react-native'

import { get, findIndex, isFunction, isEmpty, isEqual } from 'lodash'


import OverlayCardLabel from './OverlayCardLabel'
import EventCard from './EventCard'

class EventsList extends Component {
    componentWillReceiveProps(nextProps) {
        if (!this.carousel) {
            return
        }

        const nextSelectedItem = get(nextProps, 'selectedItem')
        const currentSelectedItem = get(this.props, 'selectedItem')
        const currentItems = get(this.props, 'items')

        if (nextSelectedItem !== currentSelectedItem) {
            const index = findIndex(currentItems, { id: nextSelectedItem.id })
            if (index !== -1 && this.carousel.currentIndex !== index) {
                this.carousel.snapToItem(index, true, false)
            }
        }
    }

    shouldComponentUpdate(nextProps) {
        if (!isEqual(get(this.props, 'items'), get(nextProps, 'items'))) {
            return true
        }

        return true
    }

    render() {
        const { items, onChange } = this.props
        const sliderWidth = Dimensions.get('window').width
        const itemWidth = sliderWidth - 80

        if (isEmpty(items)) {
            return (
                <OverlayCardLabel
                    icon="sf/parking-deny"
                    text='There are no nearby events.'
                />
            )
        }

        return (
            <Carousel
                ref={ (c) => (this.carousel = c) }
                data={ items }
                containerCustomStyle={ styles.container }
                onSnapToItem={ (index) => {
                    if (isFunction(onChange)) {
                        const item = items[index]
                        onChange(item)
                    }
                } }
                sliderWidth={ sliderWidth }
                itemWidth={ itemWidth }
                renderItem={ ({ item }) => <EventCard item={ item } /> }
                lockScrollWhileSnapping
                activeSlideOffset={ 10 }
                inactiveSlideOpacity={ 1 }
            />
        )
    }
}

EventsList.propTypes = {
    items    : PropTypes.arrayOf(PropTypes.object).isRequired,
    onChange : PropTypes.func
}

const styles = StyleSheet.create({
    container: {
        position      : 'absolute',
        left          : 0,
        bottom        : 0,
        right         : 0,
        height        : 240,
        paddingBottom : 20
    }
})

export default EventsList
