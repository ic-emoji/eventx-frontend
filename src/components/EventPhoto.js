import React from 'react'
import PropTypes from 'prop-types'

import { View, Image } from 'react-native'

import { get } from 'lodash'

import Colors from '../colors'

const EventPhoto = ({ item, isThumb }) => {
    const size = isThumb
        ? { width: 90, height: 90 }
        : { width: '100%', height: 200 }

    return (
        <View style={ { ...size, backgroundColor: Colors.lightGrey } }>
            { get(item, 'photo_urls').length > 0 && (
                <Image
                    source={ { uri: get(item, 'photo_urls')[0] } }
                    style={ size }
                />
            ) }
        </View>
    )
}

EventPhoto.propTypes = {
    item    : PropTypes.object,
    isThumb : PropTypes.bool
}

EventPhoto.defaultProps = {
    isThumb: false
}

export default EventPhoto
