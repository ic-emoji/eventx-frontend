import React from 'react'
import PropTypes from 'prop-types'
import { Platform } from 'react-native'
import { compose } from 'recompose'
import { connect } from 'react-redux'

import { permissions } from '../redux'

import BlankSlate from './BlankSlate'

const PermissionProvider = (props) => {
    const { children, permission, hasPermission, permissionStatus, dispatch } = props
    const text = `In order to start parking, you need to enable ${permission} access.`
    const buttonTitle = Platform.select({
        ios     : 'Open settings',
        android : (
            permissionStatus === permissions.STATES.restricted
                ? 'Open settings'
                : 'Request permission'
        )
    })

    if (!hasPermission) {
        return (
            <BlankSlate
                text={ text }
                buttonTitle={ buttonTitle }
                icon="sf/compass"
                onPress={ () => dispatch(permissions.request(permission, { openSettings: true })) }
            />
        )
    }

    return children
}

PermissionProvider.propTypes = {
    permission       : PropTypes.string.isRequired,
    hasPermission    : PropTypes.bool,
    permissionStatus : PropTypes.string
}

function mapStateToProps(state, ownProps) {
    const { permission } = ownProps
    const hasPermission = permissions.isGranted(permission)(state)
    const permissionStatus = permissions.getStatus(permission)(state)

    return {
        hasPermission,
        permissionStatus
    }
}

export default compose(
    connect(mapStateToProps)
)(PermissionProvider)
