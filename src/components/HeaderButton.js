import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { routing } from '../redux'

import IconButton from './IconButton'

import Colors from '../colors'

const HeaderButton = ({ side, dispatch, isFormScreen }) => {
    switch (side) {
        case 'left':
            return isFormScreen
                ? (
                    <IconButton
                        icon="sf/arrow-back"
                        color={ Colors.darkGrey }
                        onPress={ () => dispatch(routing.pop()) }
                        
                    />
                )
                : (
                    <IconButton
                        icon="sf/hamburger-alt"
                        color={ Colors.darkGrey }
                        onPress={ () => dispatch(routing.openDrawer()) }
                    />
                )

        case 'right': {
            return (
                <IconButton
                    icon="sf/map"
                    color={ Colors.darkGrey }
                    iconSize={ 22 }
                    onPress={ () => dispatch(routing.navigate({ routeName: 'map' })) }
                />
            )
        }

        default:
            return null
    }
}

HeaderButton.propTypes = {
    side     : PropTypes.oneOf(['left', 'right']),
    dispatch : PropTypes.func
}

function mapStateToProps(state) {
    const screen = routing.getCurrentScreen(state)
    const isFormScreen = routing.isFormScreen(state)

    return {
        screen,
        isFormScreen,
    }
}

export default connect(mapStateToProps)(HeaderButton)
