import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { reduxForm, getFormValues, getFormSubmitErrors } from 'redux-form'
import { omit } from 'lodash'

import forms from '../redux/forms'

function form(config) {
    const formID = config.id

    return (WrappedComponent) => {
        const Form = (props) => {
            const {
                handleSubmit,
                initialValues, submitting, dirty,
                pristine, valid, error, dispatch, ...otherProps
            } = props

            const onSubmit = handleSubmit((values) => (
                new Promise((resolve, reject) => {
                    dispatch(forms.submitForm({ form: formID, values, resolve, reject }))
                })
            ))

            const newProps = {
                ...otherProps,
                isSubmitting : submitting,
                isDirty      : dirty,
                isPristine   : pristine,
                isValid      : valid,
                initialValues,
                onSubmit,
                error,
                dispatch
            }

            return (
                <WrappedComponent {...newProps} />
            )
        }

        Form.propTypes = {
            handleSubmit  : PropTypes.func.isRequired,
            submitting    : PropTypes.bool.isRequired,
            dirty         : PropTypes.bool.isRequired,
            pristine      : PropTypes.bool.isRequired,
            valid         : PropTypes.bool.isRequired,
            error         : PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
            initialValues : PropTypes.object
        }

        function mapStateToProps(state) {
            const currentValues = getFormValues(formID, forms.getFormState)(state) || {}
            const errors = getFormSubmitErrors(formID, forms.getFormState)(state) || {}

            return {
                currentValues,
                errors
            }
        }

        const ReduxForm = reduxForm({
            form               : formID,
            enableReinitialize : true,
            destroyOnUnmount   : false,
            getFormState       : forms.getFormState,
            ...omit(config, 'id')
        })(Form)

        return connect(mapStateToProps)(ReduxForm)
    }
}

export default form
