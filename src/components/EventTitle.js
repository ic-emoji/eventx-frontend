import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, StyleSheet, ViewPropTypes } from 'react-native'

import EventIcon from './EventIcon'
import DistanceDetails from './DistanceDetails'

const EventTitle = ({ item, withIcon, withDistance, style }) => {
    if (!item) {
        return null
    }

    return (
        <View style={ [styles.container, style] }>
            <View style={ styles.titleWrapper }>
                { withIcon && <EventIcon size={ 14 } /> }
                <Text numberOfLines={ 1 } style={ styles.title }>{ item.name }</Text>
            </View>
            { withDistance && <DistanceDetails item={ item } size="large" isInline /> }
        </View>
    )
}

EventTitle.propTypes = {
    item         : PropTypes.object,
    withIcon     : PropTypes.bool,
    withDistance : PropTypes.bool,
    style        : ViewPropTypes.style
}

EventTitle.defaultProps = {
    withIcon     : false,
    withDistance : true
}

const styles = StyleSheet.create({
    container: {
        flex           : 1,
        flexDirection  : 'column',
        marginVertical : 5
    },
    titleWrapper: {
        flexDirection : 'row',
        alignItems    : 'center'
    },
    title: {
        flex             : 1,
        fontFamily       : 'Muli-Bold',
        fontSize         : 18,
        marginHorizontal : 5
    }
})

export default EventTitle
