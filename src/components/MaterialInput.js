import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { get, head, isEqual, isArray } from 'lodash'

import { TextField } from 'react-native-material-textfield'

import Colors from '../colors'

class MaterialInput extends Component {
    shouldComponentUpdate(nextProps) {
        if (!isEqual(get(this.props, 'input'), get(nextProps, 'input'))) {
            return true
        }

        if (!isEqual(get(this.props, 'meta'), get(nextProps, 'meta'))) {
            return true
        }

        if (!isEqual(get(this.props, 'label'), get(nextProps, 'label'))) {
            return true
        }

        return false
    }

    focus() {
        this.textField.focus()
    }

    blur() {
        this.textField.focus()
    }

    clear() {
        this.textField.clear()
    }

    value() {
        this.textField.value()
    }

    render() {
        const {
            input, meta, label, isLight, isLarge,
            autoCorrect, autoCapitalize, disableFullscreenUI,
            multiline, ...otherProps
        } = this.props

        const { error } = meta
        const { onChange } = input

        const colors = isLight ? {
            baseColor : Colors.grey,
            textColor : Colors.lightGrey,
            tintColor : Colors.white
        } : {
            baseColor : Colors.grey,
            textColor : Colors.darkGrey,
            tintColor : Colors.darkGrey
        }

        return (
            <TextField
                ref={ (ref) => (this.textField = ref) }
                label={ label || '' }
                onChangeText={ onChange }
                error={ isArray(error) ? head(error) : error }
                errorColor={ Colors.red }
                style={ styles.input }
                fontSize={ isLarge ? 24 : 18 }
                autoCorrect={ autoCorrect }
                autoCapitalize={ autoCapitalize }
                disableFullscreenUI={ disableFullscreenUI }
                animationDuration={ 100 }
                multiline={multiline}
                { ...colors }
                { ...input }
                { ...otherProps }
            />
        )
    }
}

MaterialInput.propTypes = {
    label               : PropTypes.string,
    input               : PropTypes.object,
    meta                : PropTypes.object,
    isLight             : PropTypes.bool,
    isLarge             : PropTypes.bool,
    autoCorrect         : PropTypes.bool,
    autoCapitalize      : PropTypes.oneOf(['none', 'sentences', 'words', 'characters']),
    disableFullscreenUI : PropTypes.bool,
    multiline           : PropTypes.bool
}

MaterialInput.defaultProps = {
    meta                : {},
    autoCorrect         : false,
    autoCapitalize      : 'none',
    disableFullscreenUI : true
}

const styles = StyleSheet.create({
    input: {
        fontFamily: 'Muli-Regular'
    }
})

export default MaterialInput
