import React from 'react'
import { connect } from 'react-redux'
import { Field } from 'redux-form'

import { forms } from '../redux'

import Input from './Input'
import StickyButton from './StickyButton'
import form from './Form'
import FormContainer from './FormContainer'

const EventForm = ({ onSubmit, isSubmitting }) => (
    <FormContainer
        button={ (
            <StickyButton
                onPress={ onSubmit }
                isSubmitting={ isSubmitting }
                title='Save'
            />
        ) }
    >
        <Field
            name="name"
            label='Name'
            component={ Input }
        />
        <Field
            name="description"
            label='Description'
            component={ Input }
            multiline
        />
    </FormContainer>
)

EventForm.propTypes = forms.propTypes

export default connect()(form({ id: forms.event.id })(EventForm))
