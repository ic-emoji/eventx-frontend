import React from 'react'
import { Field } from 'redux-form'

import { routing } from '../redux'

import form from './Form'
import FormContainer from './FormContainer'
import StickyButton from './StickyButton'
import TouchableText from './TouchableText'
import Logo from './Logo'
import Title from './Title'
import Input from './Input'

import forms from '../redux/forms'

const EmailLoginForm = ({ onSubmit, isSubmitting, dispatch }) => (
    <FormContainer
        button={(
            <StickyButton
                onPress={onSubmit}
                isSubmitting={isSubmitting}
                title='Login'
            />
        )}
    >
        <Logo />
        <Title>Login to your account</Title>
        <Field
            name="email"
            component={Input}
            placeholder='Email'
            keyboardType="email-address"
            isLarge
        />
        <Field
            name="password"
            component={Input}
            placeholder='Password'
            secureTextEntry
            isLarge
        />
        <TouchableText
            text={"Don't have an account yet?\nSignup now"}
            onPress={() => dispatch(routing.navigate({ routeName: 'signup' })) }
        />
    </FormContainer>
)

EmailLoginForm.propTypes = forms.propTypes

export default form({ id: forms.emailLogin.id })(EmailLoginForm)
