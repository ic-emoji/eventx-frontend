import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { auth } from '../redux'

import Container from './Container'
import DrawerMenuItem from './DrawerMenuItem'
import DrawerMenuUserInfo from './DrawerMenuUserInfo'

const DrawerMenu = ({ dispatch }) => {
    return (
        <Container>
            <DrawerMenuUserInfo />
            <DrawerMenuItem
                title='Events'
                icon="sf/map"
                screen="finder"
            />
            <DrawerMenuItem
                title='Profile'
                icon="sf/user"
                screen="profile"
            />
            <DrawerMenuItem
                title='Logout'
                icon="sf/logout"
                onPress={ () => dispatch(auth.logout()) }
            />
        </Container>
    )
}

DrawerMenu.propTypes = {
    dispatch: PropTypes.func.isRequired,
}

export default connect()(DrawerMenu)
