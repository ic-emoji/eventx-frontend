import React from 'react'
import PropTypes from 'prop-types'
import { compose, onlyUpdateForKeys } from 'recompose'
import { View, StyleSheet, ViewPropTypes } from 'react-native'
import { connect } from 'react-redux'

import { routing, map } from '../redux'

import IconButton from './IconButton'

import Colors from '../colors'

const TYPES = ['menu', 'map', 'center']
const ICONS = {
    menu   : { icon: 'sf/hamburger-alt', iconSize: 22 },
    map    : { icon: 'sf/map', iconSize: 20 },
    center : { icon: 'sf/center', iconSize: 22 },
}

const MapButton = (props) => {
    const { type, isActive, dispatch, style } = props

    const onPress = {
        menu   : routing.openDrawer(),
        map    : routing.navigate({ routeName: 'finder' }),
        center : isActive ? map.disableUserFollow() : map.enableUserFollow(),
    }

    return (
        <View style={ [styles.container, style, styles[type], isActive && styles.active] }>
            <IconButton
                color={ isActive ? Colors.white : Colors.darkGrey }
                onPress={ () => dispatch(onPress[type]) }
                { ...ICONS[type] }
            />
        </View>
    )
}

MapButton.propTypes = {
    type     : PropTypes.oneOf(TYPES),
    style    : ViewPropTypes.style,
    isActive : PropTypes.bool,
    dispatch : PropTypes.func
}

const styles = StyleSheet.create({
    container: {
        position        : 'absolute',
        width           : 40,
        height          : 40,
        borderRadius    : 20,
        backgroundColor : Colors.white,
        shadowColor     : Colors.black,
        shadowOpacity   : 0.1,
        shadowOffset    : { width: 2, height: 4 },
        shadowRadius    : 4,
        elevation       : 3,
        alignItems      : 'center',
        justifyContent  : 'center'
    },
    active: {
        backgroundColor: Colors.blue
    },
    menu: {
        left : 12,
        top  : 35
    },
    map: {
        left : 12,
        top  : 35
    },
    center: {
        right : 12,
        top   : 35
    }
})

function mapStateToProps(state, ownProps) {
    const { type } = ownProps
    const isActive = type === 'center' && map.isFollowingUser(state)

    return {
        isActive
    }
}

export default compose(
    connect(mapStateToProps),
    onlyUpdateForKeys(['type', 'isActive'])
)(MapButton)
