import React from 'react'
import { StyleSheet, ViewPropTypes } from 'react-native'
import PropTypes from 'prop-types'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Colors from '../colors'

const FormContainer = ({ children, button, style }) => ([
    <KeyboardAwareScrollView
        key="scrollView"
        extraScrollHeight={ 110 }
        keyboardShouldPersistTaps="handled"
        style={ styles.container }
        contentContainerStyle={ style }
        enableOnAndroid
    >
        { children }
    </KeyboardAwareScrollView>,
    React.isValidElement(button) && React.cloneElement(button, { key: 'submitButton' })
])

FormContainer.propTypes = {
    button : PropTypes.node,
    style  : ViewPropTypes.style
}

const styles = StyleSheet.create({
    container: {
        flex              : 1,
        paddingHorizontal : 20,
        backgroundColor   : Colors.white
    }
})

export default FormContainer
