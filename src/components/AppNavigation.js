import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { BackHandler } from 'react-native'
import { useScreens } from 'react-native-screens'
import { setRootViewBackgroundColor } from 'react-native-root-view-background'

import { app, routing } from '../redux'
import Colors from '../colors'

class AppNavigation extends Component {
    componentDidMount() {
        const { dispatch } = this.props
        setRootViewBackgroundColor(Colors.white)
        useScreens()
        dispatch(app.initialize())
        BackHandler.addEventListener('hardwareBackPress', () => this.onBackPress())
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', () => this.onBackPress())
    }

    onBackPress() {
        const { dispatch, canGoBack } = this.props

        if (!canGoBack) {
            BackHandler.exitApp()
            return true
        }

        dispatch(routing.back())
        return true
    }

    render() {
        const { routes, dispatch } = this.props
        const { ReduxNavigator } = routing
        return <ReduxNavigator state={ routes } dispatch={ dispatch } />
    }
}

AppNavigation.propTypes = {
    routes    : PropTypes.object.isRequired,
    canGoBack : PropTypes.bool.isRequired
}

function mapStateToProps(state) {
    return {
        routes    : routing.getRoutes(state),
        canGoBack : routing.canGoBack(state)
    }
}

export default connect(mapStateToProps)(AppNavigation)
