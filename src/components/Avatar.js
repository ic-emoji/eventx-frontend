import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, Text, StyleSheet } from 'react-native'

import { get } from 'lodash'

import { users } from '../redux'

import Colors from '../colors'

const Avatar = ({ profile, size }) => {
    const dimensions = { width: size, height: size, borderRadius: size / 2 }
    return (
        <View style={ [styles.container, dimensions] }>
            {
                get(profile, 'photo_urls', []).length !== 0
                    ? <Image source={ { uri: profile.photo_urls[0] } } style={ [styles.avatar, dimensions] } />
                    : <Text style={ styles.initials }>{ users.avatarInitials(profile) }</Text>
            }
        </View>
    )
}

Avatar.propTypes = {
    size    : PropTypes.number,
    profile : PropTypes.object
}

Avatar.defaultProps = {
    size: 80
}

const styles = StyleSheet.create({
    container: {
        alignItems      : 'center',
        justifyContent  : 'center',
        backgroundColor : Colors.lightGrey
    },
    initials: {
        color      : 'white',
        fontSize   : 36,
        fontWeight : '300'
    },
    avatar: {

    }
})

export default Avatar
