import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, StyleSheet, ViewPropTypes } from 'react-native'
import Text from './Text'

import Colors from '../colors'

const TouchableText = ({ text, onPress, style, withPadding, ...otherProps }) => (
    <TouchableOpacity
        onPress={ onPress }
        activeOpacity={ 0.5 }
        style={ [styles.container, withPadding && styles.padded, style] }
        { ...otherProps }
    >
        <Text style={ styles.text }>{ text }</Text>
    </TouchableOpacity>
)

TouchableText.propTypes = {
    text        : PropTypes.string,
    onPress     : PropTypes.func,
    style       : ViewPropTypes.style,
    withPadding : PropTypes.bool
}

TouchableText.defaultProps = {
    withPadding: true
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    text: {
        textAlign : 'center',
        fontSize  : 16,
        color     : Colors.darkGrey
    },
    padded: {
        padding: 25
    }
})

export default TouchableText
