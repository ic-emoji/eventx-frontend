import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'

import { map, join, get, isEmpty } from 'lodash'

import LabelWithIcon from './LabelWithIcon'

import Colors from '../colors'

const ICONS = {
    distance : 'sf/car',
    duration : 'sf/clock'
}

const DistanceDetails = ({ item, isInline }) => {
    const styles = {
        justifyContent : 'flex-start',
        flexDirection  : isInline ? 'row' : 'column'
    }
    return (
        <View style={ styles }>
            { map(['distance', 'duration'], (attribute) => {
                if (isEmpty(get(item, [attribute, 'text']))) {
                    return null
                }
                return (
                    <LabelWithIcon
                        key={ join([item.id, attribute], '-') }
                        text={ item[attribute].text }
                        icon={ ICONS[attribute] }
                        color={ Colors.grey }
                    />
                )
            }) }
        </View>
    )
}

DistanceDetails.propTypes = {
    item     : PropTypes.object,
    isInline : PropTypes.bool
}

DistanceDetails.defaultProps = {
    isInline: false
}

export default DistanceDetails
