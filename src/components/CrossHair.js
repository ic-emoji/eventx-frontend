import React from 'react'

import { View, StyleSheet } from 'react-native'
import Icon from './Icon'

import Colors from '../colors'

const CrossHair = () => (
    <View style={ styles.container } pointerEvents="none">
        <Icon name="sf/center" size={ 30 } color={ Colors.blue } />
    </View>
)

const styles = StyleSheet.create({
    container: {
        position        : 'absolute',
        left            : '50%',
        top             : '50%',
        marginTop       : -28.5,
        marginLeft      : -30,
        width           : 60,
        height          : 60,
        backgroundColor : 'transparent',
        display         : 'flex',
        alignItems      : 'center',
        justifyContent  : 'center'
    }
})

export default CrossHair
