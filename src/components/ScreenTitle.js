import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { connect } from 'react-redux'

import Text, { WEIGHT } from './Text'

import { routing } from '../redux'

import Colors from '../colors'

const ScreenTitle = ({ title }) => (
    <Text numberOfLines={ 1 } weight={ WEIGHT.extraBold } style={ styles.title }>{ title }</Text>
)

ScreenTitle.propTypes = {
    title: PropTypes.string
}

function mapStateToProps(state) {
    return {
        title: routing.getScreenTitle(state)
    }
}

const styles = StyleSheet.create({
    title: {
        flex      : 1,
        width     : '100%',
        color     : Colors.darkGrey,
        fontSize  : 30,
        textAlign : 'left'
    }
})

export default connect(mapStateToProps)(ScreenTitle)
