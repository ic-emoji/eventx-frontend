import React from 'react'
import { Field } from 'redux-form'

import { routing, forms } from '../redux'

import form from './Form'
import FormContainer from './FormContainer'
import StickyButton from './StickyButton'
import Logo from './Logo'
import Title from './Title'
import TouchableText from './TouchableText'
import Input from './Input'

const EmailSignupForm = ({ onSubmit, isSubmitting, dispatch }) => (
    <FormContainer
        button={ (
            <StickyButton
                onPress={ onSubmit }
                isSubmitting={ isSubmitting }
                title='Signup'
            />
        ) }
    >
        <Logo />
        <Title>Create a new account</Title>
        <Field
            name="email"
            component={ Input }
            placeholder='Email'
            keyboardType="email-address"
            isLarge
        />
        <Field
            name="password1"
            component={ Input }
            placeholder='Password'
            secureTextEntry
            isLarge
        />
        <Field
            name="password2"
            component={ Input }
            placeholder='Password Confirmation'
            secureTextEntry
            isLarge
        />
        <TouchableText
            text={'Already have an account?\nLog in'}
            onPress={ () => dispatch(routing.navigate({ routeName: 'login' })) }
        />
    </FormContainer>
)

EmailSignupForm.propTypes = forms.propTypes

export default form({ id: forms.emailSignup.id })(EmailSignupForm)
