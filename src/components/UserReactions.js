import React from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'

import { get } from 'lodash'

import { users, events } from '../redux'
import utils from '../utils'

import Button from './Button'
import Text from './Text'

import Colors from '../colors';

const UserReactions = ({ isInterested, isGoing, event, currentUser, going, interested, dispatch }) => {
    const react = (interested, going) => dispatch({
        type    : events.USER_REACTED,
        payload : { event_id: event.id, interested, going, user_id: currentUser.id }
    })
    const goingBtnStyle = { backgroundColor: isGoing ? Colors.darkOrange : Colors.blue, width: 85 }
    const interestedBtnStyle = { backgroundColor: isInterested ? Colors.darkOrange : Colors.blue, width: 85 }

    return (
        <View>
            <View style={styles.horizontalContainer}>
                <Button
                    title='Going'
                    onPress={() => react(false, !isGoing)}
                    isRounded
                    small
                    style={goingBtnStyle}
                />
                <Button
                    title='Interested'
                    onPress={() => react(!isInterested, false)}
                    isRounded
                    small
                    style={interestedBtnStyle}
                />
            </View>
            <View style={styles.horizontalContainer}>
                <Text>{`Going: ${going}`}</Text>
                <Text>{`Interested: ${interested}`}</Text>
            </View>
        </View>
    )
}

const styles = {
    horizontalContainer: {
        flex           : 0.7,
        flexDirection  : 'row',
        justifyContent : 'space-between',
        margin         : 25
    }
}

function mapStateToProps(state, currentProps) {
    const { event } = currentProps
    const currentUser = users.getCurrent(state)
    const currentUserReaction = utils.getCurrentUserReaction(currentUser, event)

    const isInterested = get(currentUserReaction, 'interested', false)
    const isGoing = get(currentUserReaction, 'going', false)

    const { going, interested } = utils.getReactionsNumber(event)
    return {
        isInterested,
        isGoing,
        event,
        currentUser,
        going,
        interested
    }
}

export default connect(mapStateToProps)(UserReactions)
