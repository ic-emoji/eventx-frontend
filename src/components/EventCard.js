import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { View, StyleSheet } from 'react-native'

import Card from './Card'
import Overlay from './Overlay'
import EventPhoto from './EventPhoto'
import EventTitle from './EventTitle'
import EventButton from './EventButton'

import { routing } from '../redux'

const EventCard = (props) => {
    const { item, dispatch } = props
    return (
        <Overlay withPadding={ false }>
            <Card
                onPress={ () => {
                    dispatch(routing.navigate({ routeName: 'eventView', params: { id: item.id, isForm: false } }))
                } }
            >
                <View style={ styles.content }>
                    <EventPhoto item={ item } isThumb />
                    <EventTitle
                        style={ styles.details }
                        item={ item }
                        withAvailability
                        withDistance
                    />
                </View>
                <EventButton item={ item } />
            </Card>
        </Overlay>
    )
}

EventCard.propTypes = {
    item: PropTypes.object
}

const styles = StyleSheet.create({
    content: {
        flexDirection : 'row',
        marginBottom  : 20,
        height        : 100
    },
    details: {
        alignSelf  : 'center',
        marginLeft : 10
    }
})

export default connect()(EventCard)
