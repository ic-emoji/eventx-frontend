import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, StyleSheet } from 'react-native'

import { ui } from '../redux'

import Container from './Container'
import LabelWithIcon from './LabelWithIcon'
import Button from './Button'

const BlankSlate = ({ text, icon, buttonTitle, onPress }) => (
    <Container withPadding>
        <TouchableOpacity
            name={ icon }
            onPress={ onPress }
            style={ styles.container }
        >
            <LabelWithIcon
                text={ text }
                icon={ icon }
                size={ ui.SIZE.xLarge }
            />
            <Button
                title={ buttonTitle }
                onPress={ onPress }
                style={ styles.button }
                intent={ ui.INTENT.default }
            />
        </TouchableOpacity>
    </Container>
)


BlankSlate.propTypes = {
    text        : PropTypes.string,
    buttonTitle : PropTypes.string,
    icon        : PropTypes.node,
    onPress     : PropTypes.func
}

const styles = StyleSheet.create({
    container: {
        margin         : 20,
        flex           : 1,
        justifyContent : 'center'
    },
    button: {
        top: 20
    }
})

export default BlankSlate
