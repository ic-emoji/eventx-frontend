import React from 'react'
import { connect } from 'react-redux'
import { Field } from 'redux-form'

import { forms } from '../redux'

import Input from './Input'
import StickyButton from './StickyButton'
import form from './Form'
import FormContainer from './FormContainer'
import UserRating from './UserRating';

const ProfileForm = ({ onSubmit, isSubmitting, initialValues }) => (
    <FormContainer
        button={ (
            <StickyButton
                onPress={ onSubmit }
                isSubmitting={ isSubmitting }
                title='Save'
            />
        ) }
    >
        <UserRating
            currentUser={ initialValues }
        />
        <Field
            name="first_name"
            label='First name'
            component={ Input }
        />
        <Field
            name="last_name"
            label='Last name'
            component={ Input }
        />
        <Field
            name="email"
            label='Email Address'
            keyboardType="email-address"
            component={ Input }
        />
        <Field
            name="phone"
            label='Phone'
            component={ Input }
        />
    </FormContainer>
)

ProfileForm.propTypes = forms.propTypes

export default connect()(form({ id: forms.profile.id })(ProfileForm))
