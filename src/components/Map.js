/* eslint-disable global-require */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, Dimensions, ViewPropTypes } from 'react-native'
import { connect } from 'react-redux'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import Color from 'color'

import {
    map as _map, get, join, take, values, throttle, debounce,
    isString, isFunction, isObject, isEqual
} from 'lodash'


import mapStyle from '../assets/mapStyle'
import { map, geolocation, routing } from '../redux'

class Map extends Component {
    constructor(props) {
        super(props)
        this.state = { isReady: false }
    }

    componentWillReceiveProps(nextProps) {
        if (!this.map) {
            return
        }

        if (!get(this.state, 'isReady')) {
            return
        }

        const nextRegion = get(nextProps, 'region')
        const currentRegion = get(this.props, 'region')

        if (isObject(nextRegion) && nextRegion !== currentRegion) {
            this.map.animateToRegion(nextRegion, 200)
        }
    }

    shouldComponentUpdate(nextProps) {
        if (!isEqual(get(this.props, 'markers'), get(nextProps, 'markers'))) {
            return true
        }

        if (!isEqual(get(this.props, 'polygons'), get(nextProps, 'polygons'))) {
            return true
        }

        return false
    }

    render() {
        const {
            markers, polygons, region, style, isInteractive, showsTraffic, dispatch,
            onMarkerPress, onMapPress
        } = this.props

        const { isReady } = this.state

        const height = Platform.select({
            android : (Dimensions.get('window').height - routing.TABBAR_HEIGHT) - 20,
            ios     : (Dimensions.get('window').height - routing.TABBAR_HEIGHT) + 30
        })

        return (
            <MapView
                ref={ (ref) => (this.map = ref) }
                provider={ PROVIDER_GOOGLE }
                style={ [{ width: '100%', height, flex: 1 }, style] }
                showsTraffic={ showsTraffic }
                showsUserLocation
                showsMyLocationButton={ false }
                followsUserLocation
                loadingEnabled
                rotateEnabled={ false }
                pitchEnabled={ false }
                zoomEnabled={ isInteractive }
                zoomControlEnabled={ isInteractive }
                cacheEnabled={ !isInteractive }
                scrollEnabled={ isInteractive }
                initialRegion={ region }
                customMapStyle={ mapStyle }
                paddingAdjustmentBehavior="never"
                onMapReady={ () => this.setState({ isReady: true }) }
                onRegionChangeComplete={ debounce((region) => {
                    dispatch(map.updateMap({ region }))
                }, 100, { leading: false, trailing: true }) }
                onUserLocationChange={ throttle((e) => {
                    if (e.nativeEvent) {
                        const coords = e.nativeEvent.coordinate
                        dispatch(geolocation.updatePosition({ coords }))
                    }
                }, 5000) }
                onMoveShouldSetResponder={ debounce(() => {
                    dispatch(map.drag())
                }, 50, { leading: true, trailing: false }) }
                onPress={ (e) => {
                    e.stopPropagation()
                    if (isFunction(onMapPress)) {
                        const { coordinate } = e.nativeEvent
                        onMapPress(coordinate)
                    }
                } }
            >
                { isReady && _map(markers, (marker) => (
                    <MapView.Marker
                        key={ marker.identifier }
                        identifier={ marker.identifier }
                        coordinate={ marker.position }
                        image={ require('../assets/images/map-marker-selected.png') }
                        style={ marker.isSelected ? { zIndex: 999 } : {} }
                        onPress={ (e) => {
                            e.stopPropagation()
                            if (isFunction(onMarkerPress)) {
                                onMarkerPress(marker)
                            }
                        } }
                    />
                )) }
                { isReady && _map(polygons, ({ identifier, color, coordinates }) => (
                    <MapView.Polygon
                        key={ identifier }
                        coordinates={ coordinates }
                        fillColor={ color }
                        strokeColor={ color }
                    />
                )) }
            </MapView>
        )
    }
}

Map.propTypes = {
    markers       : PropTypes.arrayOf(PropTypes.object),
    polygons      : PropTypes.arrayOf(PropTypes.object),
    region        : PropTypes.object,
    style         : ViewPropTypes.style,
    onMarkerPress : PropTypes.func,
    onMapPress    : PropTypes.func,
    isInteractive : PropTypes.bool,
    showsTraffic  : PropTypes.bool
}

Map.defaultProps = {
    isInteractive : true,
    showsTraffic  : true
}

function mapStateToProps(state, ownProps) {
    const markers = _map(ownProps.markers, (marker) => {
        const identifier = join([marker.name, ...values(marker.position)])
        return {
            ...marker,
            identifier
        }
    })

    const polygons = _map(ownProps.polygons, (polygon) => {
        const color = isString(polygon.color)
            ? Color(polygon.color).alpha(polygon.isSelected ? 0.6 : 0.2).string()
            : 'transparent'

        const identifier = join(_map(take(polygon.coordinates, 2), values))

        return {
            ...polygon,
            identifier,
            color
        }
    })

    return {
        markers,
        polygons,
        region: map.getRegion(state)
    }
}

export default connect(mapStateToProps)(Map)
