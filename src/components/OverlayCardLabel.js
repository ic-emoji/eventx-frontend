import React from 'react'
import PropTypes from 'prop-types'
import { onlyUpdateForKeys } from 'recompose'

import Card from './Card'
import Overlay from './Overlay'
import LabelWithIcon from './LabelWithIcon'

const OverlayCardLabel = (props) => {
    const { icon, text } = props
    return (
        <Overlay>
            <Card>
                <LabelWithIcon icon={ icon } text={ text } />
            </Card>
        </Overlay>
    )
}

OverlayCardLabel.propTypes = {
    text : PropTypes.string,
    icon : PropTypes.string
}

export default onlyUpdateForKeys(['item'])(OverlayCardLabel)
