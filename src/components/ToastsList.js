import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose, onlyUpdateForKeys } from 'recompose'
import { View, StyleSheet } from 'react-native'

import { map, toArray } from 'lodash'

import { ui } from '../redux'

import Toast from './Toast'

const ToastsList = (props) => {
    const { items, dispatch } = props
    return (
        <View style={ styles.container }>
            { map(items, ({ id, message, ...otherProps }) => (
                <Toast
                    key={ id }
                    onClose={ () => dispatch(ui.closeToast(id)) }
                    message={ message }
                    { ...otherProps }
                />
            ))}
        </View>
    )
}

ToastsList.propTypes = {
    dispatch : PropTypes.func.isRequired,
    items    : PropTypes.arrayOf(PropTypes.object)
}

function mapStateToProps(state) {
    return {
        items: toArray(ui.getToasts(state))
    }
}

const styles = StyleSheet.create({
    container: {
        position       : 'absolute',
        width          : '100%',
        flex           : 1,
        top            : 25,
        justifyContent : 'space-between',
        zIndex         : 1001
    }
})

export default compose(
    connect(mapStateToProps),
    onlyUpdateForKeys(['items'])
)(ToastsList)
