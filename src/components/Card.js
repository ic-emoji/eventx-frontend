import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableHighlight, StyleSheet } from 'react-native'

import { isFunction } from 'lodash'

import Colors from '../colors'

const Card = (props) => {
    const { children, onPress, ...otherProps } = props
    return (
        <TouchableHighlight
            style={ styles.container }
            onPress={ onPress }
            disabled={ !isFunction(onPress) }
            delayPressIn={ 300 }
            underlayColor={ Colors.lightestGrey }
            { ...otherProps }
        >
            <View>{ children }</View>
        </TouchableHighlight>
    )
}

Card.propTypes = {
    onPress: PropTypes.func
}

const styles = StyleSheet.create({
    container: {
        flexDirection   : 'column',
        justifyContent  : 'space-between',
        borderRadius    : 5,
        padding         : 10,
        flex            : 1,
        overflow        : 'hidden',
        backgroundColor : Colors.white
    }
})

export default Card
