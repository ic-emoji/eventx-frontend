import React from 'react'
import { BottomTabBar } from 'react-navigation-tabs'
import { KeyboardAwareTabBarComponent } from 'react-native-keyboard-accessory'

const RouterTabBar = (props) => (
    <KeyboardAwareTabBarComponent TabBarComponent={ BottomTabBar } { ...props } />
)

export default RouterTabBar
