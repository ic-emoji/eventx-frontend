import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose, onlyUpdateForKeys } from 'recompose'
import { join } from 'lodash'

import { View, StyleSheet } from 'react-native'

import { users } from '../redux'

import Text, { WEIGHT } from './Text'
import Avatar from './Avatar'

import Colors from '../colors'

const DrawerMenuUserInfo = (props) => {
    const { currentUser } = props
    return (
        <View style={ styles.container }>
            <Avatar profile={ currentUser } />
            <View style={ styles.textContainer }>
                <Text weight={ WEIGHT.bold } style={ styles.name }>
                    { join([currentUser.first_name, currentUser.last_name], ' ') }
                </Text>
                <Text style={ styles.email }>{ currentUser.email || currentUser.phone }</Text>
            </View>
        </View>
    )
}

DrawerMenuUserInfo.propTypes = {
    currentUser: PropTypes.object
}

const styles = StyleSheet.create({
    container: {
        padding       : 20,
        flexDirection : 'column'
    },
    name: {
        fontSize: 24
    },
    email: {
        fontSize : 14,
        color    : Colors.grey
    },
    textContainer: {
        marginVertical: 20
    }
})

function mapStateToProps(state) {
    return {
        currentUser: users.getCurrent(state)
    }
}

export default compose(
    connect(mapStateToProps),
    onlyUpdateForKeys(['currentUser'])
)(DrawerMenuUserInfo)
