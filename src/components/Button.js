import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity, StyleSheet, ViewPropTypes, ActivityIndicator } from 'react-native'
import { values, isString, isEmpty } from 'lodash'
import { onlyUpdateForKeys } from 'recompose'

import Icon from './Icon'
import Text, { WEIGHT } from './Text'
import { INTENT } from '../redux/ui'

import Colors from '../colors'

const Button = (props) => {
    const {
        children, title, icon, intent, onPress, style, width,
        isRounded, isDisabled, isLoading, isSmall, ...otherProps
    } = props

    return (
        <TouchableOpacity
            style={ [
                styles.baseButton,
                styles[intent],
                style,
                isRounded && styles.rounded,
                isDisabled && styles.disabled,
                isSmall && styles.smallButton,
                width && { width, maxWidth: width }
            ] }
            onPress={ onPress }
            activeOpacity={ 0.5 }
            disabled={ isDisabled || isLoading }
            { ...otherProps }
        >
            <View style={ styles.container }>
                <View
                    style={ [
                        styles.iconWrapper,
                        isEmpty(title) && { width: '100%' },
                        isSmall && styles.smallIconWrapper
                    ] }
                >
                    { isLoading && <ActivityIndicator color="white" /> }
                    { !isLoading && isString(icon) && <Icon name={ icon } style={ styles.icon } size={ 25 } /> }
                    {
                        !isLoading
                        && React.isValidElement(icon)
                        && React.cloneElement(icon, { style: styles.icon, ...icon.props, size: 20 })
                    }
                </View>
                { title && <Text weight={ WEIGHT.bold } style={ styles.label }>{ title }</Text> }
                { children }
            </View>
        </TouchableOpacity>
    )
}


Button.propTypes = {
    icon       : PropTypes.node,
    title      : PropTypes.string,
    intent     : PropTypes.oneOf(values(INTENT)),
    onPress    : PropTypes.func,
    style      : ViewPropTypes.style,
    isDisabled : PropTypes.bool,
    isLoading  : PropTypes.bool,
    isRounded  : PropTypes.bool,
    isSmall    : PropTypes.bool,
    width      : PropTypes.number
}

Button.defaultProps = {
    intent     : INTENT.primary,
    isDisabled : false,
    isLoading  : false,
    isRounded  : true,
    isSmall    : false
}

const HEIGHT = {
    large : 54,
    small : 40
}

const styles = StyleSheet.create({
    baseButton: {
        flex      : 1,
        height    : HEIGHT.large,
        maxHeight : HEIGHT.large,
        minHeight : HEIGHT.large
    },
    smallButton: {
        height    : HEIGHT.small,
        maxHeight : HEIGHT.small,
        minHeight : HEIGHT.small
    },
    rounded: {
        borderRadius : 5,
        overflow     : 'hidden'
    },
    primary: {
        backgroundColor: Colors.darkOrange
    },
    success: {
        backgroundColor: Colors.orange
    },
    danger: {
        backgroundColor: Colors.red
    },
    warning: {
        backgroundColor: Colors.orange
    },
    default: {
        backgroundColor: Colors.blue
    },
    disabled: {
        opacity: 0.5
    },
    container: {
        flex           : 1,
        flexDirection  : 'row',
        justifyContent : 'flex-start'
    },
    iconWrapper: {
        width          : HEIGHT.large,
        height         : '100%',
        marginRight    : -HEIGHT.large,
        alignItems     : 'center',
        justifyContent : 'center'
    },
    smallIconWrapper: {
        width       : HEIGHT.small,
        marginRight : -HEIGHT.small
    },
    icon: {
        color: Colors.white
    },
    label: {
        flex              : 1,
        textAlign         : 'center',
        textAlignVertical : 'center',
        alignSelf         : 'center',
        fontSize          : 18,
        color             : Colors.white,
        backgroundColor   : 'transparent'
    }
})

export default onlyUpdateForKeys(['title', 'icon', 'isLoading', 'isDisabled', 'onPress'])(Button)
