import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import Icon from './Icon'

import Colors from '../colors'

const ParkingLotIcon = ({ size }) => (
    <View
        style={ {
            borderRadius    : size / 3,
            backgroundColor : Colors.blue,
            width           : 1.5 * size,
            height          : 1.5 * size,
            alignItems      : 'center',
            justifyContent  : 'center',
            overflow        : 'hidden'
        } }
    >
        <Icon
            name="sf/parking"
            size={ size + 5 }
            style={ { color: 'white' } }
        />
    </View>
)

ParkingLotIcon.propTypes = {
    size: PropTypes.number
}

ParkingLotIcon.defaultProps = {
    size: 22
}

export default ParkingLotIcon
