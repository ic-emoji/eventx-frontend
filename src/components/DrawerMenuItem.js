import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose, onlyUpdateForKeys } from 'recompose'

import { View, TouchableOpacity, StyleSheet } from 'react-native'

import { isString, isFunction } from 'lodash'

import { routing } from '../redux'

import Icon from './Icon'
import Text, { WEIGHT } from './Text'

import Colors from '../colors'

const DrawerMenuItem = (props) => {
    const { title, icon, screen, params, isActive, onPress, dispatch } = props

    return (
        <TouchableOpacity
            name={ icon }
            onPress={ () => {
                dispatch(routing.closeDrawer())
                if (isFunction(onPress)) {
                    onPress()
                }
                if (isString(screen)) {
                    dispatch(routing.navigate({ routeName: screen, params }))
                }
            } }
            style={ styles.button }
        >
            <View style={ styles.iconContainer }>
                <Icon
                    name={ icon }
                    size={ 22 }
                    color={ isActive ? Colors.blue : Colors.lightGrey }
                />
            </View>
            <Text weight={ WEIGHT.semiBold } style={ styles.label }>{ title }</Text>
        </TouchableOpacity>
    )
}


DrawerMenuItem.propTypes = {
    title    : PropTypes.string,
    icon     : PropTypes.node,
    screen   : PropTypes.string,
    params   : PropTypes.object,
    onPress  : PropTypes.func,
    isActive : PropTypes.bool
}

const styles = StyleSheet.create({
    button: {
        flexDirection   : 'row',
        alignItems      : 'center',
        height          : 50,
        backgroundColor : 'transparent'
    },
    iconContainer: {
        width      : 50,
        marginTop  : 5,
        alignItems : 'center'
    },
    label: {
        color    : Colors.black,
        fontSize : 16
    }
})

function mapStateToProps(state, { screen }) {
    const currentScreen = routing.getCurrentScreen(state)
    const parentScreen = routing.getParentScreen(state)
    const isActive = screen === currentScreen || screen === parentScreen

    return {
        isActive
    }
}

export default compose(
    connect(mapStateToProps),
    onlyUpdateForKeys(['title', 'isActive'])
)(DrawerMenuItem)
