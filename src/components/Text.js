import React from 'react'
import PropTypes from 'prop-types'
import { Text as BaseText } from 'react-native'

import { values } from 'lodash'

export const WEIGHT = {
    extraLight : '-ExtraLight',
    light      : '-Light',
    regular    : '-Regular',
    bold       : '-Bold',
    semiBold   : '-SemiBold',
    extraBold  : '-ExtraBold'
}

const Text = ({ children, weight, style, ...otherProps }) => (
    <BaseText
        style={ [style, { fontFamily: `Muli${weight}` }] }
        { ...otherProps }
    >
        { children }
    </BaseText>
)

Text.propTypes = {
    weight : PropTypes.oneOf(values(WEIGHT)),
    style  : BaseText.propTypes.style
}

Text.defaultProps = {
    weight: WEIGHT.regular
}

export default Text
