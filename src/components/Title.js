import React from 'react'
import { StyleSheet } from 'react-native'

import Text, { WEIGHT } from './Text'

import Colors from '../colors'

const Title = (props) => (
    <Text weight={ WEIGHT.regular } style={ styles.title } { ...props } />
)

const styles = StyleSheet.create({
    title: {
        fontSize  : 24,
        textAlign : 'center',
        color     : Colors.darkGrey
    }
})

export default Title
