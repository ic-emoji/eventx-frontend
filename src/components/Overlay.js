import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'

import Container from './Container'

import Colors from '../colors'

const Overlay = (props) => {
    const { children, withPadding } = props
    return (
        <Container withPadding={ withPadding } style={ styles.container }>
            { children }
        </Container>
    )
}

Overlay.propTypes = {
    withPadding: PropTypes.bool
}

Overlay.defaultProps = {
    withPadding: true
}

const styles = StyleSheet.create({
    container: {
        position        : 'absolute',
        bottom          : 0,
        left            : 0,
        width           : '100%',
        backgroundColor : 'transparent',
        shadowColor     : Colors.black,
        shadowOpacity   : 0.2,
        shadowOffset    : { width: 0, height: 6 },
        shadowRadius    : 24,
        elevation       : 4
    }
})

export default Overlay
