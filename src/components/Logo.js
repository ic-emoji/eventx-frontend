import React from 'react'
import { View, Image, StyleSheet } from 'react-native'

const Logo = () => (
    <View style={ styles.container }>
        <Image
            style={ styles.image }
            // eslint-disable-next-line
            source={ require('../assets/images/logo.png') }
        />
    </View>
)

const styles = StyleSheet.create({
    container: {
        alignSelf      : 'center',
        marginVertical : 50
    }
})

export default Logo
