import React from 'react'
import { Image, View } from 'react-native'

import { get } from 'lodash'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Title from './Title'
import Text from './Text'
import UserReactions from './UserReactions'

import Colors from '../colors'

const EventDetails = ({ event }) => (
    <KeyboardAwareScrollView
        key="scrollView"
        extraScrollHeight={ 110 }
        keyboardShouldPersistTaps="handled"
        enableOnAndroid
    >
        <Title>{get(event, 'name')}</Title>
        <UserReactions event={event} />
        { get(event, 'photo_urls', []).length > 0 && (
            <View style={styles.container}>
                <Image
                    source={ { uri: get(event, 'photo_urls')[0] } }
                    style={ styles.image }
                />
            </View>
        ) }
        <Text style={styles.description} multiline>{get(event, 'description')}</Text>
    </KeyboardAwareScrollView>
)

const styles = {
    image: {
        justifyContent : 'center',
        alignItems     : 'center',
        width          : 300,
        height         : 300
    },
    container: {
        justifyContent : 'center',
        alignItems     : 'center',
    },
    description: {
        fontSize : 17,
        margin   : 20,
        color    : Colors.grey
    }
}

export default EventDetails
