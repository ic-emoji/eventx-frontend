import React from 'react'
import PropTypes from 'prop-types'

import { get } from 'lodash'

import Container from './Container'
import Icon from './Icon'

import Color from '../colors'

const UserRating = ({ currentUser }) => {
    let starRating = get(currentUser.rating, 'star_rating', 0)
    starRating = Math.round(starRating)

    const stars = []
    for (let i = 0; i < starRating; i += 1) {
        stars.push(<Icon key={i} name='sf/star' size={50} color={Color.google} />)
    }

    return (
        <Container flex='row'>
            { stars }
        </Container>
    )
}

UserRating.propTypes = {
    currentUser: PropTypes.object
}

export default UserRating
