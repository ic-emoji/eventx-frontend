import React from 'react'
import PropTypes from 'prop-types'

import Text from './Text'

const TabBarLabel = ({ tab }) => (
    <Text style={ { textAlign: 'center' } }>{ tab.label }</Text>
)

TabBarLabel.propTypes = {
    tab: PropTypes.object
}

export default TabBarLabel
