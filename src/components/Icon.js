import React from 'react'
import PropTypes from 'prop-types'

import { has, split } from 'lodash'

import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import Octoicons from 'react-native-vector-icons/Octicons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Zocial from 'react-native-vector-icons/Zocial'

import SafeFleet from '../assets/fonts/SafeFleet'

const ICONS = {
    sf : SafeFleet,
    fa : FontAwesome,
    md : MaterialIcons,
    sl : SimpleLineIcons,
    go : Octoicons,
    fe : Feather,
    io : Ionicons,
    zo : Zocial
}

const Icon = ({ name, ...otherProps }) => {
    const [pack, iconName] = split(name, '/')

    if (!has(ICONS, pack)) {
        return null
    }

    return React.createElement(ICONS[pack], { name: iconName, ...otherProps })
}

Icon.propTypes = {
    name: PropTypes.string.isRequired
}

export default Icon
