import React from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet } from 'react-native'

import { values, isString } from 'lodash'

import { ui } from '../redux'

import Icon from './Icon'
import Text, { WEIGHT } from './Text'

import Colors from '../colors'

const LabelWithIcon = ({ children, text, description, icon, color, size }) => {
    const iconSize = {
        default : 18,
        xl      : 36
    }

    const textWeight = {
        lg : WEIGHT.bold,
        xl : WEIGHT.bold
    }

    return (
        <View style={ [styles.container, styles[`${size}Container`]] }>
            <View style={ [styles.labelContainer, styles[`${size}LabelContainer`]] }>
                <View style={ [styles.iconContainer, {}] }>
                    { React.isValidElement(icon) && React.cloneElement(icon, { style: styles.icon, ...icon.props }) }
                    { isString(icon) && (
                        <Icon
                            style={ [styles.icon, styles[`${size}Icon`], { color }] }
                            name={ icon }
                            size={ iconSize[size] || 18 }
                        />
                    ) }
                </View>
                <Text
                    style={ [styles.label, styles[`${size}Label`], { color }] }
                    weight={ textWeight[size] || WEIGHT.regular }
                >
                    { text }
                </Text>
            </View>
            { description && (
                <Text style={ styles.description }>
                    { description }
                </Text>
            ) }
            { children }
        </View>
    )
}

LabelWithIcon.propTypes = {
    text        : PropTypes.string,
    description : PropTypes.string,
    icon        : PropTypes.node,
    color       : PropTypes.string,
    size        : PropTypes.oneOf(values(ui.SIZE))
}

LabelWithIcon.defaultProps = {
    size: ui.SIZE.default
}

const styles = StyleSheet.create({
    container: {
        flexDirection : 'row',
        marginRight   : 10
    },
    labelContainer: {
        flexDirection : 'row',
        alignItems    : 'center'
    },
    icon: {
        padding: 3
    },
    label: {
        fontSize: 14
    },
    description: {
        fontSize : 14,
        color    : Colors.grey
    },
    lgIcon: {
        padding: 8
    },
    lgLabel: {
        fontSize: 16
    },
    lgContainer: {
        flexDirection: 'column'
    },
    xlContainer: {
        justifyContent: 'center'
    },
    xlLabelContainer: {
        flexDirection: 'column'
    },
    xlIcon: {
        padding: 10
    },
    xlLabel: {
        fontSize  : 20,
        textAlign : 'center'
    }
})

export default LabelWithIcon
