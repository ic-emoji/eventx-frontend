import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, StyleSheet, ViewPropTypes } from 'react-native'
import Icon from './Icon'

const IconButton = ({ icon, color, iconSize, onPress, style }) => (
    <TouchableOpacity
        style={ [styles.button, style] }
        activeOpacity={ 0.5 }
        onPress={ onPress }
    >
        <Icon
            name={ icon }
            color={ color }
            size={ iconSize }
            backgroundColor="transparent"
        />
    </TouchableOpacity>
)

IconButton.propTypes = {
    icon     : PropTypes.string,
    color    : PropTypes.string,
    onPress  : PropTypes.func,
    iconSize : PropTypes.number,
    style    : ViewPropTypes.style
}

IconButton.defaultProps = {
    iconSize: 25
}

const styles = StyleSheet.create({
    button: {
        flex           : 1,
        width          : 50,
        height         : 50,
        alignItems     : 'center',
        justifyContent : 'center'
    }
})

export default IconButton
