import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import utils from '../utils'

import Button from './Button'

import { ui, nearby, geolocation } from '../redux'

const EventButton = (props) => {
    const { item, dispatch } = props

    const openNavigation = () => dispatch(geolocation.getDirections({ destination: utils.latLng(item) }))

    return (
        <Button
            icon="sf/compass"
            title='Go Now'
            onPress={ openNavigation }
            intent={ ui.INTENT.default }
        />
    )
}

EventButton.propTypes = {
    item: PropTypes.object.isRequired,
}

function mapStateToProps(state, ownProps) {
    const { item } = ownProps
    const currentPosition = geolocation.getCurrentPosition(state)

    const isNearby = nearby.isInProximity(item.url)(state)

    return {
        item,
        currentPosition,
        isNearby,
    }
}

export default connect(mapStateToProps)(EventButton)
