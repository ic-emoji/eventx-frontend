import React from 'react'

import Container from '../components/Container'

import EmailLoginForm from '../components/EmailLoginForm'

const EmailLoginScreen = () => (
    <Container>
        <EmailLoginForm />
    </Container>
)


EmailLoginScreen.navigationOptions = {
    header: null
}

export default EmailLoginScreen
