import EmailLoginScreen from './EmailLoginScreen'
import EmailSignupScreen from './EmalSignupScreen'
import MapScreen from './MapScreen'
import AccountSettingsScreen from './AccountSettingsScreen'
import EventScreen from './EventScreen'

export default {
    EmailLoginScreen,
    EmailSignupScreen,
    MapScreen,
    AccountSettingsScreen,
    EventScreen
}
