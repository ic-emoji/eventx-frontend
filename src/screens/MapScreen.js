import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { head, has } from 'lodash'

import { map, nearby, permissions } from '../redux'
import utils from '../utils'

import Container from '../components/Container'
import PermissionProvider from '../components/PermissionProvider'
import Map from '../components/Map'
import MapButton from '../components/MapButton'
import EventsList from '../components/EventsList'
import AddEventMapButton from '../components/AddEventMapButton'

const MapScreen = (props) => {
    const {
        mapRegion, events,
        selectedEvent, dispatch
    } = props

    return (
        <Container>
            <PermissionProvider permission={ permissions.LOCATION }>
                <Container>
                    <Map
                        markers={ utils.asMarkers(events) }
                        onMarkerPress={ (marker) => dispatch(map.selectMarker(marker)) }
                        region={ mapRegion }
                    />
                    <MapButton type="menu" />
                    <MapButton type="center" />
                    <AddEventMapButton type="event" />
                    <EventsList
                        items={ events }
                        selectedItem={ selectedEvent }
                        onChange={ (item) => dispatch(map.selectMarker(item)) }
                    />
                </Container>
            </PermissionProvider>
        </Container>
    )
}

MapScreen.propTypes = {
    mapRegion     : PropTypes.object,
    events        : PropTypes.arrayOf(PropTypes.object),
    selectedEvent : PropTypes.object
}

MapScreen.navigationOptions = {
    header: null
}

function mapStateToProps(state) {
    const mapRegion = map.getRegion(state)

    const events = nearby.getAllSorted(state)

    const selectedMarker = map.getSelectedMarker(state)
    const selectedEvent = has(selectedMarker, 'id')
        ? nearby.getForID(selectedMarker.id)(state)
        : head(events)

    return {
        mapRegion,
        events,
        selectedEvent,
    }
}

export default connect(mapStateToProps)(MapScreen)
