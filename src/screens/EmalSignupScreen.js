import React from 'react'

import Container from '../components/Container'
import EmailSignupForm from '../components/EmailSignupForm'


const EmailSignupScreen = () => (
    <Container>
        <EmailSignupForm />
    </Container>
)

EmailSignupScreen.navigationOptions = {
    header: null
}

export default EmailSignupScreen
