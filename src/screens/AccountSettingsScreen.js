import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose, onlyUpdateForKeys } from 'recompose'

import { users } from '../redux'

import Container from '../components/Container'
import ProfileForm from '../components/ProfileForm'

const AccountSettingsScreen = (props) => {
    const { currentUser } = props

    return (
        <Container>
            <ProfileForm initialValues={ currentUser } />
        </Container>
    )
}

function mapStateToProps(state) {
    return {
        currentUser: users.getCurrent(state)
    }
}

AccountSettingsScreen.propTypes = {
    currentUser: PropTypes.object
}

export default compose(
    connect(mapStateToProps),
    onlyUpdateForKeys(['currentUser'])
)(AccountSettingsScreen)
