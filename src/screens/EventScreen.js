import React from 'react'
import { connect } from 'react-redux'

import { isEmpty } from 'lodash'

import { routing, nearby } from '../redux'

import Container from '../components/Container'
import EventForm from '../components/EventForm'
import EventDetails from '../components/EventDetails'

const EventScreen = ({ isForm, event }) => {
    return (
        <Container>
            { isForm ? <EventForm /> : <EventDetails event={event} /> }
        </Container>
    )
}

function mapStateToProps(state) {
    const params = routing.getParams(state)
    if (!isEmpty(params)) {
        const { isForm, id } = params
        let event
        if (isForm === false) {
            event = nearby.getForID(id)(state)
        }

        return {
            isForm,
            event
        }
    }

    return {
    }
}

export default connect(mapStateToProps)(EventScreen)
