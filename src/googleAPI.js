import axios from 'axios'
import { reduce, join, isArray } from 'lodash'

const googleAPI = axios.create({
    baseURL : 'https://maps.googleapis.com/maps/api/',
    params  : { key: 'AIzaSyDdNaKFBOA_FlJa6hlUZxxVGyrzDlvsOM4' }
})

googleAPI.interceptors.request.use((config) => ({
    ...config,
    url    : join([config.url, 'json'], '/'),
    params : reduce(config.params, (acc, param, key) => ({
        ...acc,
        [key]: isArray(param) ? join(param, '|') : param
    }), {})
}))

export default googleAPI
