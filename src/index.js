/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import { Provider } from 'react-redux';

import store from './redux/store';
import AppNavigation from './components/AppNavigation'
import ToastsList from './components/ToastsList'

const App = () => {
    return (
        <Provider store={store}>
            <AppNavigation />
            <ToastsList />
        </Provider>
    )
}

export default App
