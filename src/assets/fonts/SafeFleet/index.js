import { createIconSet } from 'react-native-vector-icons'
import glyphMap from './glyphMap'

export default createIconSet(glyphMap, 'SafeFleet', 'SafeFleet.ttf')
