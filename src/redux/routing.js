import React from 'react'
import {
    createAppContainer, createSwitchNavigator, createStackNavigator, createDrawerNavigator,
    NavigationActions, DrawerActions, StackActions
} from 'react-navigation'

import {
    createReactNavigationReduxMiddleware, createNavigationReducer,
    createReduxContainer
} from 'react-navigation-redux-helpers'

import { createSelector } from 'reselect'

import { Keyboard } from 'react-native'
import SplashScreen from 'react-native-splash-screen'

import { takeLatest, put, select } from 'redux-saga/effects'

import { get, find, startCase, endsWith } from 'lodash'

import { auth, app } from '.'

import screens from '../screens'

import DrawerMenu from '../components/DrawerMenu'
import HeaderButton from '../components/HeaderButton'
import ScreenTitle from '../components/ScreenTitle'
import RouterTabBar from '../components/RouterTabBar'
import TabBarLabel from '../components/TabBarLabel'

import Icon from '../components/Icon'

import Colors from '../colors'

export const {
    navigate,
    back,
    setParams,
    BACK,
    NAVIGATE
} = NavigationActions

export const {
    push,
    pop,
    popToTop,
    replace,
    reset,
    POP
} = StackActions

export const {
    openDrawer,
    closeDrawer,
    toggleDrawer,
    OPEN_DRAWER,
    CLOSE_DRAWER,
    TOGGLE_DRAWER
} = DrawerActions

export const TABS = {
    account: {
        label : 'Account Settings',
        icon  : 'sf/role-user'
    },
    street: {
        label : 'Street Parking',
        icon  : 'sf/car'
    },
    private: {
        label : 'Private Parking',
        icon  : 'sf/barrier-open'
    }
}

export const TABBAR_HEIGHT = 118
export const NAVBAR_HEIGHT = 100

export const NAVIGATION_OPTIONS = {
    tabs: {
        defaultNavigationOptions: (state) => {
            const { routeName } = state.navigation.state
            const tab = get(TABS, routeName, {})
            return ({
                tabBarIcon: (state) => (
                    <Icon name={ tab.icon } size={ 30 } color={ state.tintColor } />
                ),
                tabBarLabel: () => <TabBarLabel tab={ tab } />
            })
        },
        tabBarOptions: {
            activeTintColor         : Colors.blue,
            inactiveTintColor       : Colors.darkGrey,
            activeBackgroundColor   : Colors.white,
            inactiveBackgroundColor : Colors.white,
            labelStyle              : {
                fontSize   : 14,
                fontFamily : 'Muli-Regular',
                color      : Colors.darkGrey
            },
            style: {
                height          : TABBAR_HEIGHT,
                backgroundColor : Colors.white,
                borderTopColor  : Colors.white,
                shadowColor     : Colors.black,
                shadowOpacity   : 0.2,
                shadowOffset    : { width: 0, height: 6 },
                shadowRadius    : 24,
                borderTopWidth  : 0,
                elevation       : 4
            },
            tabStyle: {
                paddingVertical: 30
            }
        },
        tabBarComponent: RouterTabBar
    },
    stack: {
        headerMode               : 'screen',
        animationEnabled         : true,
        defaultNavigationOptions : {
            gesturesEnabled : false,
            headerLeft      : <HeaderButton side="left" />,
            headerRight     : <HeaderButton side="right" />,
            headerTitle     : <ScreenTitle />,
            headerTintColor : Colors.darkGrey,
            headerStyle     : {
                backgroundColor   : Colors.white,
                borderBottomWidth : 0,
                height            : NAVBAR_HEIGHT
            },
            headerLeftContainerStyle: {
                position : 'absolute',
                left     : 7,
                top      : -40
            },
            headerRightContainerStyle: {
                position : 'absolute',
                right    : 7,
                top      : -40
            },
            headerTitleContainerStyle: {
                position : 'absolute',
                left     : 20,
                right    : 20,
                bottom   : -45
            }
        }
    }
}

export const middleware = createReactNavigationReduxMiddleware((state) => state.routing)

//
//  NAVIGATORS
const FinderStack = createStackNavigator({
    map: screens.MapScreen
}, NAVIGATION_OPTIONS.stack)

const ProfileStack = createStackNavigator({
    account: screens.AccountSettingsScreen
}, NAVIGATION_OPTIONS.stack)

const EventStack = createStackNavigator({
    eventView: screens.EventScreen
}, NAVIGATION_OPTIONS.stack)

const MainNavigation = createDrawerNavigator({
    finder  : { screen: FinderStack },
    profile : { screen: ProfileStack },
    event   : { screen: EventStack }
}, {
    contentComponent : DrawerMenu,
    initialRoute     : 'finder',
    backBehavior     : 'initialRoute'
})

const AuthStack = createStackNavigator({
    login  : { screen: screens.EmailLoginScreen },
    signup : { screen: screens.EmailSignupScreen }
}, NAVIGATION_OPTIONS.stack)

export const RootNavigator = createSwitchNavigator({
    login : { screen: AuthStack },
    main  : { screen: MainNavigation }
}, {
    initialRouteName: 'login'
})

export const ReduxNavigator = createReduxContainer(RootNavigator)
export const MainNavigator = createAppContainer(RootNavigator)

//
//  REDUCER

export const reducer = createNavigationReducer(RootNavigator)

//
//  SAGA

export function* saga() {
    yield takeLatest([
        app.INITIALIZED,
        app.ACTIVATED,
        auth.LOGOUT,
        auth.ACCESS_GRANTED,
        auth.TOKEN_EXPIRED
    ], ensureAuthentication)

    yield takeLatest([OPEN_DRAWER, TOGGLE_DRAWER], closeKeyboard)
}

function* ensureAuthentication() {
    const isAuthenticated = yield select(auth.isAuthenticated)
    const rootScreen = yield select(getRootScreen)

    if (!isAuthenticated && rootScreen !== 'login') {
        yield put(navigate({ routeName: 'login' }))
    } else if (isAuthenticated && (rootScreen === 'login' || rootScreen === 'signup')) {
        yield put(navigate({ routeName: 'main' }))
    }

    SplashScreen.hide()
}

function closeKeyboard() {
    Keyboard.dismiss()
}

//
//  SELECTORS

export const getRoutes = (state) => get(state, 'routing')

export const getRootScreen = createSelector(
    getRoutes,
    (navigationState) => {
        if (!navigationState) {
            return null
        }

        const route = navigationState.routes[navigationState.index]

        return route.routeName
    }
)

export const getCurrentRoute = createSelector(
    getRoutes,
    (navigationState) => {
        if (!navigationState) {
            return null
        }

        const route = navigationState.routes[navigationState.index]

        if (route.routes) {
            return getCurrentRoute({ routing: route })
        }

        return route
    }
)

export const getCurrentScreen = createSelector(
    getCurrentRoute,
    (route) => route.routeName
)

export const getParams = createSelector(
    getCurrentRoute,
    (route) => get(route, 'params', {})
)

export const getParentScreen = createSelector(
    [getRoutes, getCurrentScreen],
    (navigationState, currentScreen) => {
        const route = navigationState.routes[navigationState.index]

        if (route.routes) {
            if (find(route.routes, { routeName: currentScreen })) {
                return route.routeName
            }

            return getParentScreen({ routing: route })
        }
        return null
    }
)

export const getScreenTitle = (state) => {
    const currentScreen = getCurrentScreen(state)

    switch (currentScreen) {
        case 'map':
        case 'finder':
            return ''

        case 'account':
            return 'Account Settings'

        default:
            return startCase(currentScreen)
    }
}

export const canGoBack = createSelector(
    getCurrentScreen,
    (currentScreen) => {
        switch (currentScreen) {
            case 'login':
            case 'map':
            case 'finder':
                return false

            default:
                return true
        }
    }
)

export const isFormScreen = createSelector(
    getCurrentScreen,
    (screen) => endsWith(screen, 'Form')
)
