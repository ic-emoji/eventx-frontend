import { takeEvery, put, take, race, fork, call, select } from 'redux-saga/effects'
import { pick, get, find, findKey } from 'lodash'
import jwtDecode from 'jwt-decode'
import { createSelector } from 'reselect'

import forms from './forms'
import api from './api'
import * as ui from './ui'

//
// ENDPOINTS

const EMAIL_LOGIN_ENDPOINT                 = '/auth/email/login/'
const EMAIL_SIGNUP_ENDPOINT                = '/auth/email/signup/'

//
//  ACTIONS

export const LOGOUT                                = '@ auth / LOGOUT'

export const LOGIN_SUCCEEDED                       = '@ auth / LOGIN_SUCCEEDED'
export const LOGIN_FAILED                          = '@ auth / LOGIN_FAILED'

export const SIGNUP_SUCCEEDED                      = '@ auth / SIGNUP_SUCCEEDED'
export const SIGNUP_FAILED                         = '@ auth / SIGNUP_FAILED'

export const TOKEN_REFRESH_SUCCEEDED               = '@ auth / TOKEN_REFRESH_SUCCEEDED'
export const TOKEN_REFRESH_FAILED                  = '@ auth / TOKEN_REFRESH_FAILED'

export const EMAIL_VERIFICATION_REQUESTED          = '@ auth / EMAIL_VERIFICATION_REQUESTED'
export const EMAIL_VERIFICATION_SUCCEEDED          = '@ auth / EMAIL_VERIFICATION_SUCCEEDED'
export const EMAIL_VERIFICATION_FAILED             = '@ auth / EMAIL_VERIFICATION_FAILED'

export const ACCESS_GRANTED                        = '@ auth / ACCESS_GRANTED'
export const TOKEN_EXPIRED                         = '@ auth / TOKEN_EXPIRED'

export const logout = () => ({ type: LOGOUT })

const AUTH_METHODS = {
    email: {
        form     : forms.emailLogin,
        endpoint : EMAIL_LOGIN_ENDPOINT,
        payload  : ['email', 'password']
    }
}

//
// REDUCER

export function reducer(state = {}, action = {}) {
    switch (action.type) {
        case TOKEN_REFRESH_SUCCEEDED:
        case LOGIN_SUCCEEDED:
        case SIGNUP_SUCCEEDED: {
            const token = action.payload
            if (tokenIsValid(token)) {
                return {
                    ...state,
                    token
                }
            }
            return state;
        }
        case LOGOUT:
        case TOKEN_EXPIRED:
            return {
                ...state,
                token: null
            }

        default:
            return state;
    }
}

//
// SAGAS

export function* saga() {
    yield takeEvery(forms.emailLogin.SUBMITTED, handleAuthenticationForm)
    yield takeEvery(forms.emailSignup.SUBMITTED, handleSignupForm)

    yield takeEvery([LOGIN_SUCCEEDED, SIGNUP_SUCCEEDED], grantAccess)
}

function* authenticate(method, data) {
    const payload = pick(data, method.payload)
    try {
        const response = yield call(api.http.post, method.endpoint, payload)
        const token = tokenFromResponse(response)
        yield put({ type: LOGIN_SUCCEEDED, payload: token })
    } catch (errorResponse) {
        const error = api.parseErrors(errorResponse)
        yield put({ type: LOGIN_FAILED, payload: { error } })
    }
}

function* handleAuthenticationForm({ type, payload }) {
    const { values, resolve, reject } = payload

    const isAlreadyAuthenticated = yield select(isAuthenticated)
    if (isAlreadyAuthenticated) {
        return
    }

    const method = getMethodForType(AUTH_METHODS, type)

    yield fork(authenticate, method, values)

    const { success, failure } = yield race({
        success : take(LOGIN_SUCCEEDED),
        failure : take(LOGIN_FAILED)
    })
    if (success) {
        yield call(resolve)
    } else {
        const errors = api.parseErrors(failure)
        yield call(reject, new forms.SubmissionError(errors))
        yield put(ui.showToast({
            message : get(errors, '_error', 'Failed to log in.'),
            intent  : ui.INTENT.error
        }))
    }
}

function* handleSignupForm({ payload }) {
    try {
        if (payload.password1 !== payload.password2) {
            yield put({ type: SIGNUP_FAILED })
            return
        }
        const adaptedPayload = {
            email    : payload.email,
            password : payload.password1
        }
        const response = yield call(api.http.post, EMAIL_SIGNUP_ENDPOINT, adaptedPayload)
        const token = tokenFromResponse(response)
        yield put({ type: SIGNUP_SUCCEEDED, payload: token })
    } catch (errorResponse) {
        const error = api.parseErrors(errorResponse)
        yield put({ type: SIGNUP_FAILED, payload: { error } })
    }
}

function* grantAccess({ payload }) {
    yield call(api.setHeaders, { Authorization: headerFromToken(payload) })
    yield put({ type: ACCESS_GRANTED })
}

//
//   HELPERS and UTILITIES

export function tokenIsValid(token) {
    return (token && token.exp && (Date.now() / 1000) < token.exp) || false
}

function getMethodForType(methods, type) {
    const method = find(methods, (method) => get(method, 'form.SUBMITTED') === type)
    const key = findKey(methods, method)
    return {
        ...method,
        key
    }
}

export function tokenFromResponse(response) {
    const responseBody = response && response.data
    const tokenString = responseBody && responseBody.token
    return (tokenString && tokenPayload(tokenString)) || null
}

export function tokenPayload(tokenString) {
    try {
        const token = jwtDecode(tokenString)
        return { ...token, token: tokenString }
    } catch (e) {
        return null
    }
}

export function headerFromToken(tokenObject) {
    return `Token ${tokenObject.token}`
}

//
// SELECTORS

export const getState = (state) => (state.auth)
export const getToken = createSelector(
    getState,
    (state) => state.token
)
export const isAuthenticated = createSelector(
    getToken,
    (token) => tokenIsValid(token)
)
