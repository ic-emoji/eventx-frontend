import { AppState } from 'react-native'
import { put, fork, takeLatest, select, call, takeEvery } from 'redux-saga/effects'
import { createSelector } from 'reselect'
import { isEqual } from 'lodash'
import { channel as createChannel } from 'redux-saga'

import utils from '../utils'

import * as auth from './auth'

export const STATES = {
    active     : 'active',
    background : 'background',
    inactive   : 'inactive'
}

//
//  ACTIONS

export const INITIALIZED       = '@ app / INITIALIZED'
export const STATE_CHANGED = '@ app / STATE_CHANGED'
export const ACTIVATED     = '@ app / ACTIVATED'

export const initialize = () => ({ type: INITIALIZED })

//
//  REDUCER

const intialState = {
    state         : null,
    isInitialized : false
}

export function reducer(state = intialState, action = {}) {
    switch (action.type) {
        case INITIALIZED:
            return {
                ...state,
                isInitialized: true
            }

        case STATE_CHANGED: {
            return {
                ...state,
                state: action.payload
            }
        }

        default:
            return state
    }
}

//
// SAGAS

const channel = createChannel()

export function* saga() {
    yield fork(utils.watchChannel, channel)
    yield takeLatest(INITIALIZED, watchForStateChanges)
    yield takeEvery(auth.LOGOUT, reinitialize)
}

function* watchForStateChanges() {
    yield call(handleStateChange, AppState.currentState)
    AppState.addEventListener('change', handleStateChange)
}

function* handleStateChange(nextState) {
    const currentState = yield select(getCurrentState)

    if (!isEqual(currentState, nextState)) {
        yield put({ type: STATE_CHANGED, payload: nextState })

        if (nextState === STATES.active) {
            yield put({ type: ACTIVATED })
        }
    }
}

function* reinitialize() {
    yield put(initialize())
}

//
//  SELECTORS

export const getState = (state) => state.app
export const isInitialized = createSelector(
    getState,
    (app) => !!app.isInitialized
)
export const getCurrentState = createSelector(
    getState,
    (app) => app.state
)
export const isActive = createSelector(
    getCurrentState,
    (currentState) => currentState === STATES.active
)
