import createAPI from './createAPI'

const API_SPEC = {
    endpoints: {
        users  : '/users/:id?/',
        events : '/events/:id?/'
    },
    mappings: {
    },
    options: {
    }
}

const BASE_URL = 'https://14cfcde1.ngrok.io/api/'

export default createAPI(BASE_URL, API_SPEC)
