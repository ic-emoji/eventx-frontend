import { channel as createChannel } from 'redux-saga'
import { createSelector } from 'reselect'
import { fork, takeLatest, race, take, select, put } from 'redux-saga/effects'
import { Platform } from 'react-native'
import Permissions from 'react-native-permissions'
import AndroidOpenSettings from 'react-native-android-open-settings'

import { reduce, forEach, get, omit, values, includes } from 'lodash'

import utils from '../utils'

import { app, auth } from '.'

export const LOCATION = 'location'
export const NOTIFICATION = 'notification'
export const STORAGE = 'storage'

const TYPES = {
    location : LOCATION,
    storage  : STORAGE
}

//
//  ACTIONS

export const REQUESTED                = '@ permissions / REQUESTED'

export const AUTHORIZED               = '@ permissions / AUTHORIZED'
export const DENIED                   = '@ permissions / DENIED'
export const RESTRICTED               = '@ permissions / RESTRICTED'
export const UNDETERMINED             = '@ permissions / UNDETERMINED'

export const SETTINGS_REQUESTED       = '@ permissions / SETTINGS_REQUESTED'

export const request = (permission, options = {}) => ({ type: REQUESTED, payload: { permission, options } })
export const openSettings = () => ({ type: SETTINGS_REQUESTED })

export const STATES = {
    requested    : 'requested',
    authorized   : 'authorized',
    denied       : 'denied',
    restricted   : 'restricted',
    undetermined : 'undetermined'
}

const ACTIONS_MAP = {
    requested    : REQUESTED,
    authorized   : AUTHORIZED,
    denied       : DENIED,
    restricted   : RESTRICTED,
    undetermined : UNDETERMINED
}

//
//  REDUCER

const initialState = reduce(TYPES, (acc, permission) => ({
    ...acc,
    [permission]: STATES.undetermined
}), {})

export function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case REQUESTED: {
            const { permission } = action.payload
            return {
                ...state,
                [permission]: STATES.requested
            }
        }

        case AUTHORIZED:
        case DENIED:
        case RESTRICTED:
        case UNDETERMINED: {
            const { permission, result } = action.payload
            return {
                ...state,
                [permission]: get(STATES, result, STATES.undetermined)
            }
        }

        default:
            return state
    }
}


//
//  SAGA

const channel = createChannel()

export function* saga() {
    yield takeLatest([app.ACTIVATED, auth.ACCESS_GRANTED], checkExisting)
    yield takeLatest(REQUESTED, handleRequest)
    yield fork(utils.watchChannel, channel)
}

function checkExisting() {
    Permissions.checkMultiple(values(omit(TYPES, NOTIFICATION, STORAGE))).then((response) => {
        forEach(response, (result, permission) => {
            const type = ACTIONS_MAP[result]

            if (!type) {
                return
            }

            channel.put({ type, payload: { permission, result } })
        })
    })
}

function handleRequest({ payload }) {
    const { permission } = payload

    if (!includes(values(TYPES), permission)) {
        return
    }

    if (permission === STORAGE && Platform.OS !== 'android') {
        return
    }

    Permissions.request(permission).then((result) => {
        const type = ACTIONS_MAP[result]

        if (!type) {
            return
        }

        channel.put({ type, payload: { permission, result } })

        if (Platform.OS === 'android' && result === STATES.restricted) {
            AndroidOpenSettings.locationSourceSettings()
            return
        }

        if (Platform.OS === 'ios' && result === STATES.denied && Permissions.canOpenSettings()) {
            Permissions.openSettings()
        }
    })
}

export function* ensure(permission) {
    Permissions.check(permission).then((result) => {
        const type = ACTIONS_MAP[result]
        if (!type) {
            return
        }

        channel.put({ type, payload: { permission, result } })
    })

    yield take((action) => get(action, 'payload.permission') === permission)

    const hasPermission = yield select(isGranted(permission))
    if (!hasPermission) {
        yield put(request(permission))

        const { failure } = yield race({
            success: take((action) => (
                get(action, 'payload.permission') === permission && get(action, 'type') === AUTHORIZED
            )),
            failure: take((action) => (
                get(action, 'payload.permission') === permission && includes([DENIED, RESTRICTED], get(action, 'type'))
            ))
        })

        if (failure) {
            return false
        }
    }

    return true
}

//
//  SELECTORS

export const getState = (state) => state.permissions
export const getStatus = (permission) => createSelector(
    getState,
    (state) => get(state, permission)
)
export const isGranted = (permission) => createSelector(
    getStatus(permission),
    (status) => status === STATES.authorized
)
