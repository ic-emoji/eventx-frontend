import { takeLatest, put, take, race, call, select } from 'redux-saga/effects'

import { get } from 'lodash'

import forms from './forms'
import api from './api'
import * as ui from './ui'
import * as users from './users'
import * as map from './map'

export const USER_REACTED = '@ events / USER_REACTED'

export function reducer(state = {}, action = {}) {
    return api.reduceRequests('events', state, action)
}

export function* saga() {
    yield takeLatest(forms.event.SUBMITTED, handleEventCreation)
    yield takeLatest(USER_REACTED, handleUserReacted)
}

function* handleEventCreation({ payload }) {
    const { values, resolve, reject } = payload

    const currentUser = yield select(users.getCurrent)
    const center = yield select(map.getCenter)

    const newValues = {
        ...values,
        user_id : currentUser.id,
        lat     : center.latitude,
        lng     : center.longitude
    }

    const eventsURL = api.buildURL('events')
    yield put(api.create(eventsURL, newValues))
    const { success, failure } = yield race({
        success : take(api.events.CREATE_SUCCEEDED),
        failure : take(api.events.CREATE_FAILED)
    })

    if (success) {
        yield call(resolve)
        yield put(ui.showToast({
            message : 'Event created successfully!',
            intent  : ui.INTENT.success
        }))
    } else {
        const errors = api.parseErrors(failure)
        yield call(reject, new forms.SubmissionError(errors))
        yield put(ui.showToast({
            message : get(errors, '_error', 'Failed to create event.'),
            intent  : ui.INTENT.error
        }))
    }
}

function* handleUserReacted({ payload }) {
    const eventReactionURL = api.buildURL('events', { id: 'reaction' })
    yield put(api.create(eventReactionURL, payload))

    const { success } = yield race({
        success : take(api.events.CREATE_SUCCEEDED),
        failure : take(api.events.CREATE_FAILED)
    })

    if (success) {
        yield put(ui.showToast({
            message : 'You reacted successfully',
            intent  : ui.INTENT.success
        }))
        yield put({ type: '@ nearby / FETCH_REQUESTED' })
    } else {
        yield put(ui.showToast({
            message : 'Could not react.',
            intent  : ui.INTENT.error
        }))
    }
}

//
// SELECTORS

export const { getAll, getForID, getForURL, countAll } = api.events.selectors
