import { put, race, delay, take, takeLatest, select } from 'redux-saga/effects'
import { createSelector } from 'reselect'
import geolib from 'geolib'

import { head, has, isEmpty } from 'lodash'

import { app, geolocation, nearby } from '.'

import utils from '../utils'
import * as settings from '../settings'

//
//  ACTIONS

export const REGION_UPDATED          = '@ map / REGION_UPDATED'
export const MARKER_SELECTED         = '@ map / MARKER_SELECTED'
export const FOLLOW_ENABLED          = '@ map / FOLLOW_ENABLED'
export const FOLLOW_DISABLED         = '@ map / FOLLOW_DISABLED'
export const VIEW_MODE_UPDATED       = '@ map / VIEW_MODE_UPDATED'
export const REGION_UPDATE_CANCELLED = '@ map / REGION_UPDATE_CANCELLED'
export const MAP_UPDATED             = '@ map / MAP_UPDATED'
export const MAP_DRAGGED             = '@ map / MAP_DRAGGED'

export const selectMarker = (marker) => ({ type: MARKER_SELECTED, payload: marker })
export const updateViewMode = (mode) => ({ type: VIEW_MODE_UPDATED, payload: mode })
export const enableUserFollow = () => ({ type: FOLLOW_ENABLED })
export const disableUserFollow = () => ({ type: FOLLOW_DISABLED })
export const updateRegion = (region) => ({ type: REGION_UPDATED, payload: region })
export const cancelRegionUpdate = () => ({ type: REGION_UPDATE_CANCELLED })
export const updateMap = (payload) => ({ type: MAP_UPDATED, payload })
export const drag = () => ({ type: MAP_DRAGGED })

//
//  REDUCER

const initialState = {
    center : utils.latLng(settings.DEFAULT_STARTING_COORDINATES),
    region : utils.regionWithRadius({
        ...utils.latLng(settings.DEFAULT_STARTING_COORDINATES),
        radius: 6
    }),
    selectedMarker : null,
    viewMode       : 'street',
    isDragged      : false
}

export function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case REGION_UPDATED: {
            const region = action.payload
            const radius = utils.radiusFromRegion(region)
            const center = utils.latLng(region)

            return {
                ...state,
                center,
                region,
                radius
            }
        }

        case MARKER_SELECTED: {
            return {
                ...state,
                selectedMarker  : action.payload,
                isFollowingUser : false
            }
        }

        case FOLLOW_ENABLED: {
            return {
                ...state,
                isFollowingUser: true
            }
        }

        case FOLLOW_DISABLED: {
            return {
                ...state,
                isFollowingUser: false
            }
        }

        case VIEW_MODE_UPDATED: {
            return {
                ...state,
                viewMode: action.payload
            }
        }

        case MAP_DRAGGED: {
            return {
                ...state,
                isDragged: true
            }
        }

        case MAP_UPDATED: {
            return {
                ...state,
                isDragged: false
            }
        }

        default:
            return state
    }
}

export function* saga() {
    yield takeLatest(app.ACTIVATED, startUserFollow)
    yield takeLatest([FOLLOW_ENABLED, geolocation.POSITION_UPDATED], followUserIfRequired)
    yield takeLatest(MARKER_SELECTED, centerAroundMarker)
    yield takeLatest(VIEW_MODE_UPDATED, navigateWhereRequired)
    yield takeLatest(MAP_UPDATED, handleMapUpdate)
    yield takeLatest(MAP_DRAGGED, handleMapDrag)
}

function* startUserFollow() {
    yield put(enableUserFollow())
}

function* followUserIfRequired() {
    const shouldFollowUserLocation = yield select(isFollowingUser)
    if (!shouldFollowUserLocation) {
        return
    }

    const currentPosition = yield select(geolocation.getCurrentPosition)
    if (!currentPosition) {
        return
    }

    const isBeingDragged = yield select(isDragged)
    if (isBeingDragged) {
        return
    }

    const currentRegion = yield select(getRegion)

    const region = utils.regionWithRadius({ ...utils.latLng(currentPosition), radius: 0.05 })

    const distance = geolib.getDistance(
        utils.latLng(currentPosition),
        utils.latLng(currentRegion)
    )

    if (distance > 0 || utils.didChangeZoom(currentRegion, region)) {
        yield put(updateRegion(region))
    }
}

function* centerAroundMarker({ payload }) {
    if (!payload) {
        return
    }

    const isBeingDragged = yield select(isDragged)
    if (isBeingDragged) {
        return
    }

    const region = utils.regionWithRadius({ ...utils.latLng(payload), radius: 0.05 })
    yield put(updateRegion(region))
}

function* navigateWhereRequired() {
    const isBeingDragged = yield select(isDragged)
    if (isBeingDragged) {
        return
    }
    yield put(disableUserFollow())
    const nearbyParkingLots = yield select(nearby.getAllSorted())
    if (!isEmpty(nearbyParkingLots)) {
        yield put(selectMarker(head(nearbyParkingLots)))
    }
}

function* handleMapUpdate({ payload }) {
    const { cancel } = yield race({
        cancel: take([
            MAP_UPDATED,
            MAP_DRAGGED,
            REGION_UPDATED,
            REGION_UPDATE_CANCELLED,
            FOLLOW_ENABLED,
            FOLLOW_DISABLED,
            MARKER_SELECTED,
            VIEW_MODE_UPDATED
        ]),
        update: delay(500)
    })

    if (cancel) {
        return
    }

    const isBeingDragged = yield select(isDragged)
    if (isBeingDragged) {
        return
    }

    const currentRegion = yield select(getRegion)

    if (has(payload, 'region')) {
        const { region } = payload
        const distance = geolib.getDistance(
            utils.latLng(currentRegion),
            utils.latLng(region)
        )

        if (distance > 1 || utils.didChangeZoom(region, currentRegion)) {
            yield put(updateRegion(region))
        }
    }
}

function* handleMapDrag() {
    yield put(cancelRegionUpdate())
    const isFollowEnabled = yield select(isFollowingUser)
    if (isFollowEnabled) {
        yield put(disableUserFollow())
    }
}

//
//  SELECTORS

export const getState = (state) => state.map
export const getRegion = createSelector(getState, (state) => state.region)
export const getCenter = createSelector(getState, (state) => state.center)
export const getRadius = createSelector(getState, (state) => state.radius)
export const getSelectedMarker = createSelector(getState, (state) => state.selectedMarker)
export const getViewMode = createSelector(getState, (state) => state.viewMode)
export const isFollowingUser = createSelector(getState, (state) => !!state.isFollowingUser)
export const isDragged = createSelector(getState, (state) => !!state.isDragged)
