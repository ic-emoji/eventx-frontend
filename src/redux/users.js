import { createSelector } from 'reselect'
import { takeLatest, takeEvery, take, call, put, select, race } from 'redux-saga/effects'

import {
    get, has, find, join, compact, omit,
    isEmpty, toUpper, trim, split, map
} from 'lodash'

import api from './api'
import * as auth from './auth'
import forms from './forms'
import * as ui from './ui'

export const PROFILE_REQUESTED      = '@ users / PROFILE_REQUESTED'
export const PROFILE_FETCHED        = '@ users / PROFILE_FETCHED'
export const AVATAR_FETCH_REQUESTED = '@ users / AVATAR_FETCH_REQUESTED'
export const AVATAR_FETCH_SUCCEEDED = '@ users / AVATAR_FETCH_SUCCEEDED'
export const AVATAR_FETCH_FAILED    = '@ users / AVATAR_FETCH_FAILED'

//
//  ACTIONS

export const fetchProfile = () => ({ type: PROFILE_REQUESTED })

//
//  REDUCER

export function reducer(state = {}, action = {}) {
    return api.reduceRequests('users', state, action)
}

//
// SAGAS

export function* saga() {
    yield takeLatest([
        PROFILE_REQUESTED,
        auth.ACCESS_GRANTED,
        api.users.UPDATE_SUCCEEDED
    ], handleProfileRequest)

    yield takeEvery(forms.profile.SUBMITTED, handleUserProfileForm)
}

function* handleProfileRequest() {
    const isAuthenticated = yield select(auth.isAuthenticated)
    if (!isAuthenticated) {
        yield take(auth.ACCESS_GRANTED)
        yield put(fetchProfile())
        return
    }

    const profileURL = api.buildURL('users', { id: 'me' })
    yield put(api.get(profileURL))
    const response = yield call(api.waitForResponse, { url: profileURL })
    if (!response) {
        return
    }

    const payload = get(response, 'payload.response.data')
    yield put({ type: PROFILE_FETCHED, payload })
}

function* handleUserProfileForm({ payload }) {
    const { values: { url, ...attributes }, resolve, reject } = payload

    yield put(api.update(url, omit(attributes, ['photo_urls', 'addresses', 'rating'])))

    const { success, failure } = yield race({
        success : take(api.users.UPDATE_SUCCEEDED),
        failure : take(api.users.UPDATE_FAILED)
    })

    if (success) {
        yield call(resolve)
        yield put(ui.showToast({
            message : 'Your profile has been updated.',
            intent  : ui.INTENT.success
        }))
    } else {
        const errors = api.parseErrors(failure)
        yield call(reject, new forms.SubmissionError(errors))
        yield put(ui.showToast({
            message : get(errors, '_error', 'Failed update your profile.'),
            intent  : ui.INTENT.error
        }))
    }
}

//
// UTILS
export function avatarInitials(profile) {
    const initial = (string) => {
        if (isEmpty(string)) {
            return null
        }

        return toUpper(string[0])
    }

    if (!isEmpty(fullName(profile))) {
        return trim(join(compact(map(split(fullName(profile), ' '), initial)), ''))
    }

    if (has(profile, 'email')) {
        return initial(profile.email)
    }

    return null
}

export function fullName(profile) {
    return join(compact([
        get(profile, 'first_name'),
        get(profile, 'last_name')
    ]), ' ')
}

//
//  SELECTORS

export const { getAll, getForID, getForURL, countAll } = api.users.selectors

export const getCurrent = createSelector(
    [auth.getToken, getAll],
    (token, users) => {
        if (!has(token, 'email')) {
            return {}
        }

        const user = find(users, { email: token.email })
        return user || {}
    }
)
