import { takeEvery, takeLatest, call, select, put, take } from 'redux-saga/effects'
import { createSelector } from 'reselect'

import {
    map as _map, reduce, compact, pick, get, head,
    sortBy, round, indexOf, toArray, join, has, isEmpty
} from 'lodash'

import geolib from 'geolib'

import { api, geolocation, map, events, auth } from '.'
import googleAPI from '../googleAPI'
import utils from '../utils'
import * as settings from '../settings'

const DEFAULT_NEARBY_RADIUS = 50 // in km
const REFRESH_DISTANCE_TRESHOLD = 5 * 1000
const GATE_DISTANCE_TRESHOLD = 500

//
//  ACTIONS

const FETCH_REQUESTED         = '@ nearby / FETCH_REQUESTED'
const DISTANCE_DATA_RETRIEVED = '@ nearby / DISTANCE_DATA_RETRIEVED'
const SEARCH_PARAMS_UPDATED   = '@ nearby / SEARCH_PARAMS_UPDATED'

export const fetch = () => ({ type: FETCH_REQUESTED })
export const updateSearchParams = ({ params }) => ({ type: SEARCH_PARAMS_UPDATED, payload: params })

//
//  REDUCER

const initialState = {
    searchParams: {
        ...utils.latLng(settings.DEFAULT_STARTING_COORDINATES),
        radius: DEFAULT_NEARBY_RADIUS
    },
    results       : {},
    sortedResults : []
}

export function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case api.events.GET_SUCCEEDED: {
            const { data } = action.payload

            if (isEmpty(utils.latLng(state.searchParams))) {
                return state
            }

            const results = reduce(data, (acc, parkingLot) => {
                const { id } = parkingLot

                if (isEmpty(utils.latLng(parkingLot))) {
                    return acc
                }

                const distance = geolib.getDistance(
                    utils.latLng(parkingLot),
                    utils.latLng(state.searchParams)
                )

                const flightDistance = {
                    text  : `${round(distance / 1000, 1)} km`,
                    value : distance
                }

                return {
                    ...acc,
                    [id]: {
                        ...acc[id],
                        id,
                        flightDistance,
                        distance : flightDistance,
                        duration : {
                            value : get(acc, [id, 'duration', 'value'], null),
                            text  : get(acc, [id, 'duration', 'text'], '')
                        }
                    }
                }
            }, state.results)

            const sortedResults = compact(_map(sortResults(results), 'id'))

            return {
                ...state,
                results,
                sortedResults
            }
        }

        case DISTANCE_DATA_RETRIEVED: {
            if (isEmpty(action.payload)) {
                return state
            }

            const results = reduce(action.payload, (acc, result, id) => ({
                ...acc,
                [id]: {
                    ...acc[id],
                    distance     : result.distance,
                    roadDistance : result.distance,
                    duration     : result.duration
                }
            }), state.results)

            return {
                ...state,
                results,
                sortedResults: _map(sortResults(results), 'id')
            }
        }

        case SEARCH_PARAMS_UPDATED: {
            const { payload } = action

            if ((!has(payload, 'latitude') || !has(payload, 'longitude')) && !has(payload, 'radius')) {
                return state
            }

            return {
                ...state,
                searchParams: {
                    ...pick(state.searchParams, ['latitude', 'longitude', 'radius']),
                    ...payload
                }
            }
        }

        default:
            return state
    }
}

//
//  SAGA

export function* saga() {
    yield takeLatest([FETCH_REQUESTED, SEARCH_PARAMS_UPDATED, auth.ACCESS_GRANTED], fetchNearbyEvents)
    yield takeLatest(map.REGION_UPDATED, refreshSearchParamsIfNeeded)
    yield takeEvery(api.events.GET_SUCCEEDED, getDistanceInformation)
}

function* refreshSearchParamsIfNeeded() {
    const currentMapRegion = yield select(map.getRegion)
    const currentSearchParams = yield select(getSearchParams)

    const distance = geolib.getDistance(
        utils.latLng(currentMapRegion),
        utils.latLng(currentSearchParams)
    )

    if (distance > REFRESH_DISTANCE_TRESHOLD) {
        const params = utils.latLng(currentMapRegion)
        yield put(updateSearchParams({ params }))
    }
}

function* fetchNearbyEvents() {
    const { latitude, longitude, radius } = yield select(getSearchParams)
    yield put(api.get('events', {
        lat : latitude,
        lng : longitude,
        radius
    }))
}

function* getDistanceInformation({ payload }) {
    const destinations = toArray(payload.data)
    const currentPosition = yield select(geolocation.getCurrentPosition)

    if (!currentPosition) {
        yield take(geolocation.POSITION_UPDATED)
        yield call(getDistanceInformation, { payload })
        return
    }

    const serializeCoords = (coords) => join(toArray(utils.latLng(coords)), ',')

    const { data } = yield call(googleAPI.get, 'distancematrix', {
        params: {
            destinations : _map(destinations, serializeCoords),
            origins      : serializeCoords(currentPosition)
        }
    })

    const distanceData = reduce(get(data, 'rows[0].elements'), (acc, result, index) => {
        const { status, distance, duration } = result
        const { id } = destinations[index]
        if (status !== 'OK') {
            return acc
        }
        return {
            ...acc,
            [id]: { distance, duration }
        }
    }, {})

    yield put({ type: DISTANCE_DATA_RETRIEVED, payload: distanceData })
}

//
//  HELPERS AND UTILITIES

function sortResults(results) {
    return sortBy(results, ['duration.value', 'distance.value', 'flightDistance.value'])
}

//
//  SELECTORS

export const getState = (state) => state.nearby
export const getSearchParams = createSelector(
    getState,
    (state) => state.searchParams
)
export const getSortedResults = createSelector(
    getState,
    (state) => state.sortedResults
)
export const getResults = createSelector(
    getState,
    (state) => state.results
)
export const getAll = createSelector(
    [getResults, events.getAll],
    (results, events) => (
        reduce(results, (acc, result) => {
            const { id } = result
            const event = get(events, id)

            return {
                ...acc,
                [id]: {
                    ...result,
                    ...event
                }
            }
        }, {})
    )
)
export const getAllSorted = createSelector(
    [getAll, getSortedResults],
    (results, sortedResults) => sortBy(results, (result) => indexOf(sortedResults, result.id))
)
export const getForID = (id) => createSelector(
    getAll,
    (parkingLots) => parkingLots[id]
)
export const getNearest = createSelector(
    getAllSorted,
    (sortedResults) => head(sortedResults)
)

export const isInProximity = (parkingLotURL) => createSelector(
    [geolocation.getCurrentPosition, events.getForURL(parkingLotURL)],
    (currentPosition, parkingLot) => {
        if (!parkingLot || !currentPosition) {
            return false
        }

        const distance = geolib.getDistance(
            utils.latLng(parkingLot),
            utils.latLng(currentPosition.coords)
        )

        if (distance <= GATE_DISTANCE_TRESHOLD) {
            return true
        }

        return false
    }
)
