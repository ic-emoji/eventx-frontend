import { ActionSheetIOS, Platform, Linking, Alert } from 'react-native'
import { channel as createChannel } from 'redux-saga'
import { fork, takeEvery, takeLatest, put, select } from 'redux-saga/effects'
import { get, size, keys, values } from 'lodash'

import Geolocation from 'react-native-geolocation-service'

import { app, auth, permissions } from '.'

import utils from '../utils'


//
//  ACTIONS

export const POSITION_REQUESTED     = '@ geolocation / POSITION_REQUESTED'
export const WATCH_REQUESTED        = '@ geolocation / WATCH_REQUESTED'
export const WATCH_STARTED          = '@ geolocation / WATCH_STARTED'
export const WATCH_STOPPED          = '@ geolocation / WATCH_STOPPED'
export const POSITION_UPDATED       = '@ geolocation / POSITION_UPDATED'
export const POSITION_UPDATE_FAILED = '@ geolocation / POSITION_UPDATE_FAILED'
export const DIRECTIONS_REQUESTED   = '@ geolocation / DIRECTIONS_REQUESTED'

export const requestCurrentPosition = () => ({ type: POSITION_REQUESTED })
export const startPositionWatcher = () => ({ type: WATCH_REQUESTED })
export const stopPositionWatcher = () => ({ type: WATCH_STOPPED })
export const getDirections = ({ destination }) => ({
    type    : DIRECTIONS_REQUESTED,
    payload : { destination }
})
export const updatePosition = (position) => ({ type: POSITION_UPDATED, payload: { position } })

const DEFAULT_OPTIONS = {
    enableHighAccuracy : true,
    timeout            : 60000,
    distanceFilter     : 10
}

const NAVIGATION_APPS = {
    waze   : 'Waze',
    google : 'Google Maps',
    apple  : 'Apple Maps'
}

//
//  REDUCER

const initialState = {
    currentPosition : null,
    watcher         : null
}

export function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case POSITION_UPDATED: {
            const { position } = action.payload
            return {
                ...state,
                currentPosition: position
            }
        }

        case WATCH_STARTED: {
            const { watcher } = action.payload
            return {
                ...state,
                watcher
            }
        }

        case WATCH_STOPPED: {
            return {
                ...state,
                watcher: null
            }
        }

        default: return state
    }
}

//
//  SAGA

const channel = createChannel()

export function* saga() {
    yield takeLatest([
        app.ACTIVATED,
        permissions.AUTHORIZED,
        POSITION_REQUESTED
    ], obtainCurrentPosition)

    yield takeEvery(WATCH_REQUESTED, startLocationWatcher)
    yield takeEvery(WATCH_STOPPED, stopLocationWatcher)
    yield takeEvery(DIRECTIONS_REQUESTED, openExternalNavigationApp)
    yield fork(utils.watchChannel, channel)
}

function* obtainCurrentPosition() {
    const isAuthenticated = yield select(auth.isAuthenticated)
    const hasPermission = yield select(permissions.isGranted(permissions.LOCATION))

    if (!isAuthenticated || !hasPermission) {
        return
    }

    Geolocation.getCurrentPosition(
        (position) => channel.put({ type: POSITION_UPDATED, payload: { position } }),
        (error) => channel.put({ type: POSITION_UPDATE_FAILED, payload: { error } }),
        DEFAULT_OPTIONS
    )
}

function* startLocationWatcher() {
    const existingWatcher = yield select(getWatcher)
    const isAuthenticated = yield select(auth.isAuthenticated)
    const hasPermission = yield select(permissions.isGranted(permissions.LOCATION))

    if (existingWatcher || !isAuthenticated || !hasPermission) {
        return
    }

    const watcher = Geolocation.watchPosition(
        (position) => channel.put({ type: POSITION_UPDATED, payload: { position } }),
        (error) => channel.put({ type: POSITION_UPDATE_FAILED, payload: { error } }),
        DEFAULT_OPTIONS
    )
    yield put({ type: WATCH_STARTED, payload: { watcher } })
}

function* stopLocationWatcher() {
    const watcher = yield select((state) => get(state, 'geolocation.watcher'))
    if (watcher) {
        Geolocation.clearWatch(watcher)
    }
}

function openExternalNavigationApp({ payload }) {
    const { destination } = payload

    if (Platform.OS === 'android') {
        const url = createDirectionsURL(destination)
        Linking.openURL(url)
    } else {
        const appNames = values(NAVIGATION_APPS)

        ActionSheetIOS.showActionSheetWithOptions({
            title             : 'Choose a Navigation App',
            options           : [...appNames, 'Cancel'],
            cancelButtonIndex : size(appNames)
        },
        (buttonIndex) => {
            if (buttonIndex === size(appNames)) {
                return
            }

            const app = get(keys(NAVIGATION_APPS), buttonIndex)
            const url = createDirectionsURL({ app, ...destination })
            Linking.canOpenURL(url).then((canOpen) => {
                if (canOpen) {
                    Linking.openURL(url)
                } else {
                    const appName = get(values(NAVIGATION_APPS), buttonIndex)
                    Alert.alert('Error', `Failed to open ${appName}`, [{ text: 'OK' }])
                }
            })
        })
    }
}

function createDirectionsURL({ latitude, longitude, callbackURL, app }) {
    if (Platform.OS === 'android') {
        return `geo:lat,lng?q=${latitude},${longitude}?z=zoom`
    }

    switch (app) {
        case 'google':
            return `comgooglemaps-x-callback://?dll=${latitude},${longitude}&directionsmode=driving&x-source=2park.io&x-success=twopark://${callbackURL || ''}` // eslint-disable-line max-len

        case 'waze':
            return `https://waze.com/ul?ll=${latitude},${longitude}&navigate=yes`

        case 'apple':
            return `https://maps.apple.com/?daddr=${latitude},${longitude}`

        default:
            return ''
    }
}

//
//  SELECTORS

export const getCurrentPosition = (state) => state.geolocation.currentPosition
export const getWatcher = (state) => state.geolocation.watcher
