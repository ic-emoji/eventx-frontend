import { takeEvery, put, take, race, cancel, delay } from 'redux-saga/effects'

import {
    get, head, values, without, concat, omitBy, uniqueId,
    includes, isFunction, isArray, isObject, has
} from 'lodash'
import { createSelector } from 'reselect'

import * as settings from '../settings'

export const INTENT = {
    default : 'default',
    primary : 'primary',
    danger  : 'danger',
    error   : 'danger',
    warning : 'warning',
    success : 'success'
}

const DEFAULT_TITLES = {
    default : 'Success',
    primary : 'Success',
    danger  : 'Error',
    warning : 'Warning',
    success : 'Success'
}

export const SIZE = {
    default : 'default',
    xSmall  : 'xs',
    small   : 'sm',
    large   : 'lg',
    xLarge  : 'xl'
}

export const MENUS = {
    dropdown : 'dropdowns',
    sidebar  : 'sidebar'
}


//
//  ACTIONS

export const MENU_TOGGLED         = '@ ui / MENU_TOGGLED'
export const MENUS_CLOSED         = '@ ui / MENUS_CLOSED'
export const MODAL_DISPLAYED      = '@ ui / MODAL_DISPLAYED'
export const MODAL_CLOSED         = '@ ui / MODAL_CLOSED'
export const PICKER_ITEM_CHOSEN   = '@ ui / PICKER_ITEM_CHOSEN'
export const PICKER_ITEM_SELECTED = '@ ui / PICKER_ITEM_SELECTED'
export const TOAST_DISPLAYED      = '@ ui / TOAST_DISPLAYED'
export const TOAST_CLOSED         = '@ ui / TOAST_CLOSED'


export const toggleMenu = (menu, type) => ({ type: MENU_TOGGLED, payload: { type, menu } })
export const closeMenus = (type) => ({ type: MENUS_CLOSED, payload: { type } })

export const showModal = ({ title, message, intent, content }) => ({
    type    : MODAL_DISPLAYED,
    payload : { title, message, intent, content }
})
export const closeModal = () => ({ type: MODAL_CLOSED })

export const selectPickerItem = (item) => ({ type: PICKER_ITEM_SELECTED, payload: { item } })
export const choosePickerItem = (item) => ({ type: PICKER_ITEM_CHOSEN, payload: { item } })

export const showToast = ({ title, message, intent, isPersistent = false }) => ({
    type    : TOAST_DISPLAYED,
    payload : {
        id    : uniqueId(),
        title : title || DEFAULT_TITLES[intent],
        intent,
        message,
        isPersistent
    }
})
export const closeToast = (id) => ({ type: TOAST_CLOSED, payload: { id } })


//
//  REDUCER

const initialState = {
    openedMenus: {
        dropdowns : [],
        sidebar   : []
    },
    toats : {},
    modal : null
}

export function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case MENU_TOGGLED: {
            const { type, menu } = action.payload
            const { openedMenus } = state

            if (!includes(values(MENUS), type)) {
                return state
            }

            return {
                ...state,
                openedMenus: {
                    ...openedMenus,
                    [type]: includes(openedMenus[type], menu)
                        ? without(openedMenus[type], menu)
                        : concat(openedMenus[type], menu)
                }
            }
        }
        case MENUS_CLOSED: {
            const { type } = action.payload
            const { openedMenus } = state
            return {
                ...state,
                openedMenus: {
                    ...openedMenus,
                    [type]: []
                }
            }
        }
        case MODAL_DISPLAYED: {
            const { title, message, intent, content } = action.payload
            const selectedItem = isArray(content)
                ? head(content)
                : get(state, 'selectedItem')

            return {
                ...state,
                modal: {
                    title,
                    message,
                    intent,
                    content
                },
                selectedItem
            }
        }

        case MODAL_CLOSED: {
            return {
                ...state,
                modal: null
            }
        }

        case PICKER_ITEM_SELECTED: {
            const { item } = action.payload
            return {
                ...state,
                selectedItem: item
            }
        }

        case TOAST_DISPLAYED: {
            const { id, intent, title, message } = action.payload
            const timestamp = new Date().getTime()
            return {
                ...state,
                toats: {
                    ...state.toats,
                    [id]: {
                        id,
                        intent,
                        title,
                        message,
                        timestamp
                    }
                }
            }
        }

        case TOAST_CLOSED: {
            const { id } = action.payload
            return {
                ...state,
                toats: omitBy(state.toats, { id })
            }
        }

        default:
            return state
    }
}

//
//  SAGA

export function* saga() {
    yield takeEvery(TOAST_DISPLAYED, closeToastAfterDelay)
}

function* closeToastAfterDelay({ payload }) {
    if (payload.isPersistent) {
        return
    }
    yield delay(settings.TOAST_DISPLAY_DURATION * 1000)
    yield put(closeToast(payload.id))
}

export function* ensureValueIsPicked({ onSuccess, onCancel } = {}) {
    const { cancelled } = yield race({
        success   : take(PICKER_ITEM_CHOSEN),
        cancelled : take(MODAL_CLOSED)
    })

    if (cancelled) {
        if (isFunction(onCancel)) {
            onCancel()
        } else if (isObject(onCancel) && has(onCancel, 'type')) {
            yield put(onCancel)
        }

        yield cancel()
    }

    if (onSuccess) {
        if (isFunction(onSuccess)) {
            onCancel()
        } else if (isObject(onSuccess) && has(onSuccess, 'type')) {
            yield put(onSuccess)
        }
    }

    yield take(MODAL_CLOSED)
}


//
//  SELECTORS

export const getUI = (state) => (state.ui)
export const isMenuOpen = (menu, type) => createSelector(
    getUI,
    (ui) => includes(ui.openedMenus[type], menu)
)
export const getModal = createSelector(
    getUI,
    (ui) => ui.modal
)
export const getSelectedItem = createSelector(
    getUI,
    (ui) => ui.selectedItem
)
export const getToasts = createSelector(
    getUI,
    (ui) => ui.toats
)
