import { upperCase } from 'lodash'
import PropTypes from 'prop-types'

import { reducer, reset, SubmissionError, isSubmitting } from 'redux-form'

//
//  ACTIONS

const createForm = id => ({ id, SUBMITTED: `@ forms / ${upperCase(id)}_FORM_SUBMITTED` })
const submitForm = (args) => {
    const { form, ...payload } = args
    const type = descriptors[form].SUBMITTED
    return { type, payload }
}

//
//  FORM DESCRIPTORS

const descriptors = {
    emailLogin  : createForm('emailLogin'),
    emailSignup : createForm('emailSignup'),
    profile     : createForm('profile'),
    event       : createForm('event')
}

//
//   HELPERS and UTILITIES

const propTypes = {
    onSubmit      : PropTypes.func.isRequired,
    initialValues : PropTypes.object,
    currentValues : PropTypes.object,
    error         : PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    errors        : PropTypes.object,
    isDirty       : PropTypes.bool,
    isValid       : PropTypes.bool,
    isSubmitting  : PropTypes.bool,
    isPristine    : PropTypes.bool
}

//
//  SELECTORS

const getFormState = state => state.forms

export default {
    submitForm,
    reset,
    reducer,
    getFormState,
    SubmissionError,
    isSubmitting,
    propTypes,
    ...descriptors
}
