#### Tasks
This project comes with a Makefile as a more user-friendly interface to common tasks

| Target          | Description  | Environment Variables [and defaults] |
| --------------| ------------ | --------------------- |
| `make deps`   | install the dependencies required to build and run the app | - |
| `make packager`   | run the react native packager | - |
| `make lint`  | lint the source files | - |
| `make clean`  | clean builds and dependencies | - |
| `make run-ios`    | run iOS app using emulator | - |
| `make run-android`    | run android app using emulator | - |